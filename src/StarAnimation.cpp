#include "StarAnimation.hpp"

StarAnimation::StarAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("star_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("star_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("star_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("star_4")));
    addFrame(sf::Sprite(texturesDAO.getTexture("star_5")));
    addFrame(sf::Sprite(texturesDAO.getTexture("star_6")));

    updateTime = sf::milliseconds(125);

    setPosition(sf::Vector2f(x, y));

    name = "star";

    paused = false;

    angle = -10 -rand()%20;

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        angle = -angle;
        flipX();
    }


    for(auto& frame : frames){
        frame.rotate(angle);
    }

    aliveTime.restart();
}

StarAnimation::~StarAnimation(){
    //dtor
}

void StarAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction*SCREEN_LENGHT*0.02, 1));
}

bool StarAnimation::stillExists(){
    if(aliveTime.getElapsedTime().asMilliseconds() < getDuration().asMilliseconds()){
        return true;
    }
    return false;
}
