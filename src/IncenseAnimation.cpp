#include "IncenseAnimation.hpp"

IncenseAnimation::IncenseAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("incense_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("incense_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("incense_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("incense_4")));

    setPosition(sf::Vector2f(x, y));

    name = "incense";

    play();
}

IncenseAnimation::~IncenseAnimation(){
    //dtor
}
