#include "FireflyAnimation.hpp"

FireflyAnimation::FireflyAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("firefly_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("firefly_2")));

    setPosition(sf::Vector2f(x, y));

    paused = false;

    name = "firefly";

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        flipX();
    }
}

FireflyAnimation::~FireflyAnimation(){
    //dtor
}
