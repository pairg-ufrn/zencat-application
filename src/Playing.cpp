#include "playing.hpp"


Playing::Playing(Application* app, Game* game){
    this->app = app;
    this->game = game;
    game->setApp(app);
    app->statusVerifier.enable();
    app->window.setMouseCursorVisible(false);

}

Playing::~Playing(){
    app->window.setMouseCursorVisible(true);
    if(game->getGameState() == GAMESTATE::OVER){
        gameDAO.saveGame(game);
    }
    achievementsManager.saveAchievements();
}

void Playing::draw(const float dt){
    game->draw(dt);
}

void Playing::handleEvent(sf::Event& event){
    game->handleEvent(event);
}

void Playing::update(const float dt){
    game->update(dt);
    achievementsManager.trackAchievements(game);
}
