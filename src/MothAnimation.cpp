#include "MothAnimation.hpp"

MothAnimation::MothAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("moth_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("moth_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("moth_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("moth_4")));

    setPosition(sf::Vector2f(x, y));

    paused = false;

    name = "moth";

    if(getPosition().x < SCREEN_LENGHT/2){
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0)); //so the sprite do not appear from nowhere
        direction = 1;
        flipX();
    }


}

MothAnimation::~MothAnimation(){
    //dtor
}
