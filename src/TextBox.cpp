#include "TextBox.hpp"

TextBox::TextBox(){
    //ctor

    selected = false;

    shownText.setFont(fontsDAO.getFont("agency_bold"));
    shownText.setString("");
    shownText.setCharacterSize(25U);
    shownText.setColor(sf::Color::Black);

    verticalBarText.setFont(fontsDAO.getFont("agency_bold"));
    verticalBarText.setPosition(shownText.getPosition());
    verticalBarText.setCharacterSize(shownText.getCharacterSize());
    verticalBarText.setColor(shownText.getColor());
    verticalBarText.setString("|");

    body.setFillColor(sf::Color::White);
    underline.setFillColor(sf::Color::Black);

}

TextBox::TextBox(int posX, int posY, int sizeX, int sizeY){
    setSize(sizeX, sizeY);

    /*this->title.setFont(fontsDAO.getFont("agencyBold"));
    this->title.setPosition(text_x, text_y);
    this->title.setString(desc);
    this->title.setCharacterSize(20U);*/

    selected = false;

    shownText.setFont(fontsDAO.getFont("agency_bold"));
    shownText.setPosition(posX + 4, posY + 3);
    shownText.setString("");
    shownText.setCharacterSize(22U);
    shownText.setColor(sf::Color::Black);

    body.setFillColor(sf::Color::White);
    underline.setFillColor(sf::Color::Black);
    underline.setSize(sf::Vector2f(sizeX, underlineThickness));
    /*if(size == 0)
        background.setTexture(*Textures::inputBox);
    else if(size == 2)
        background.setTexture(*Textures::largeInputBox);*/

    verticalBarText.setFont(fontsDAO.getFont("agency_bold"));
    verticalBarText.setPosition(shownText.getPosition());
    verticalBarText.setCharacterSize(shownText.getCharacterSize());
    verticalBarText.setColor(shownText.getColor());
    verticalBarText.setString("|");

    background.setPosition(posX, posY);

    setPosition(posX, posY);
}


void TextBox::processUserInput(sf::Event& event){
    char letter;

    switch(event.type){

        case sf::Event::TextEntered:

            letter = static_cast<char>(event.text.unicode);

            if(letter != '\b'){ //not a backspace
                if(inputText.size() < 18){
                    if(nonDigitsOnly and not Utility::isDigit(letter)){
                        return; //If only digits are accepted and the input is not a digit...
                    }
                    else{
                        inputText.push_back(letter);
                        setShownText(inputText);
                    }

                }
            }
            else if(inputText.size() > 0){
                inputText.pop_back();
                setShownText(inputText);
            }

        break;
    }
}

void TextBox::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    target.draw(background);

    //target.draw(body);

    target.draw(shownText);

    if(selected and blinkBarClock.getElapsedTime().asMilliseconds() < blinkBarTime){
        target.draw(verticalBarText);
    }
    else if(blinkBarClock.getElapsedTime().asMilliseconds() > blinkBarTime*2){
        const_cast<sf::Clock*>(&blinkBarClock)->restart();
    }

    target.draw(underline);
}


void TextBox::action(){

}

void TextBox::select(){
    selected = true;
    blinkBarClock.restart();
}

void TextBox::deselect(){

    selected = false;
}

bool TextBox::isSelected(){
    return selected;
}

void TextBox::setTextColor(sf::Color color){
    textColor = color;
    shownText.setColor(color);
    verticalBarText.setColor(color);
}


void TextBox::setShownText(std::string input){
    inputText = input;

    shownText.setString("");

    int numberOfLetters = 0;
    int i = input.size() - 1;

    std::string aux = "";

    if(i < 0){
        //shownText.setString("|");
        return;
    }

    //The size of the text plus a constant (to keep a margin) is smaller than the size of the box
    while(shownText.getGlobalBounds().width + 16 < getSize().x && i >= 0){

       //Add the letters of the input backwards
        aux += input.at(i--);

        shownText.setString(aux);

        numberOfLetters++;

    }

    shownText.setString(getLastLetters(input, numberOfLetters));
    shownText.setPosition(sf::Vector2f(static_cast<int>(getPosition().x + getSize().x/2 - shownText.getLocalBounds().width/2),
                                       static_cast<int>(getPosition().y + getSize().y/2 - shownText.getLocalBounds().height/2)));

    verticalBarText.setPosition(shownText.getPosition().x + shownText.getLocalBounds().width + 2,
                                verticalBarText.getPosition().y);
}

void TextBox::setPosition(float x, float y){
    sf::Vector2f newPosition(x, y);

    position = newPosition;

    body.setPosition(newPosition);

    background.setPosition(newPosition);

    shownText.setPosition(x + 4, y + 3);

    verticalBarText.setPosition(shownText.getPosition().x + shownText.getLocalBounds().width + 2,
                                static_cast<int>(y + body.getLocalBounds().height/2 - verticalBarText.getLocalBounds().height/2));

    underline.setPosition(x, y + body.getSize().y + underlineThickness);
}

void TextBox::setTextAlignment(TEXTPOS opt){
    switch(opt){
        case RIGHT:
            shownText.setPosition(getPosition().x + getSize().x  - getStringSize().width,
                                  getPosition().y + getSize().y/2 - getStringSize().height/2);
        break;

        case CENTER:
            shownText.setPosition(getPosition().x + getSize().x/2 - getStringSize().width/2,
                                  getPosition().y + getSize().y/2 - getStringSize().height/2);
        break;

        case LEFT:
            shownText.setPosition(getPosition().x,
                                  getPosition().y + getSize().y/2 - getStringSize().height/2);
        break;
    }
    verticalBarText.setPosition(shownText.getPosition().x + shownText.getLocalBounds().width + 2, verticalBarText.getPosition().y);
}


sf::Vector2f TextBox::getSize(){
    return size;
}

sf::Vector2f TextBox::getPosition(){
    return position;
}

sf::FloatRect TextBox::getStringSize(){
    return shownText.getGlobalBounds();
}

std::string TextBox::getString(){
    return inputText;
}

void TextBox::setText(const std::string& text){
    inputText = text;
    setShownText(inputText);
}

std::string TextBox::getLastLetters(std::string input, int letters){
    int i;
    std::string output = "";

    if(input.size() > letters){
        for(i = input.size() - letters; i < input.size(); i++){
            output += input[i];
        }
        return output;
    }
    else return input;
}

bool TextBox::clicked(float x, float y){

    return contains(x, y);
}

bool TextBox::contains(float x, float y){
    sf::FloatRect body(getPosition().x, getPosition().y, size.x, size.y);
    return body.contains(x, y);
}

void TextBox::removeWhiteSpaces(){
    for(int i = 0; i < inputText.size(); i++){
        if(inputText.at(i) == ' '){
            inputText.erase(inputText.begin() + i);
        }
    }
}

void TextBox::setDigitsOnly(const bool& digitsOnly){
    nonDigitsOnly = digitsOnly;
}

void TextBox::removeNonDigits(){
    for(int i = 0; i < inputText.size(); i++){
        if(inputText.at(i) < '0' or inputText.at(i) > '9'){
            inputText.erase(inputText.begin() + i);
        }
    }
}



void TextBox::setSize(float x, float y){
    sf::Vector2f newSize(x, y);
    size = newSize;
    body.setSize(size);
    underline.setSize(sf::Vector2f(x, underlineThickness));
}
