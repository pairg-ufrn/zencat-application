#include "About.hpp"

About::About(Application* app) : backButton(EXIT_POSITION_X, EXIT_POSITION_Y, 48, 48, ""){

    this->app = app;

    app->statusVerifier.disable();

    backButton.setTexture(texturesDAO.getTexture("button_back"));
    backButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));

    background.setTexture(texturesDAO.getTexture("background_difficulty"));
    icon.setTexture(texturesDAO.getTexture("icon_about"));
    cat.setTexture(texturesDAO.getTexture("cat_about"));

    cat.setPosition(SCREEN_LENGHT*0.5 - cat.getTexture()->getSize().x/2, SCREEN_HEIGHT*0.55);
    icon.setPosition (SCREEN_LENGHT/2 - icon.getTexture()->getSize().x/2, static_cast<int>(SCREEN_HEIGHT*0.1));

    title.setString(translation(L"About"));
    title.setCharacterSize(42U);
    title.setColor(sf::Color::Black);
    title.setFont(fontsDAO.getFont("bromine"));
    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, SCREEN_HEIGHT*0.01);

    gameTitle.setString("Zen Cat");
    gameTitle.setCharacterSize(24U);
    gameTitle.setColor(sf::Color::Black);
    gameTitle.setFont(fontsDAO.getFont("agency_bold"));
    gameTitle.setPosition(SCREEN_LENGHT/2 - gameTitle.getLocalBounds().width/2, icon.getGlobalBounds().top + icon.getGlobalBounds().height);

    std::ifstream versionFile {"version"};
    versionFile >> versionString;

    version.setCharacterSize(24U);
    version.setString(translation(L"Version") + ": " + versionString);
    version.setColor(sf::Color::Black);
    version.setFont(fontsDAO.getFont("agency_bold"));
    version.setPosition(SCREEN_LENGHT*0.5 - version.getGlobalBounds().width*0.5,
                        gameTitle.getGlobalBounds().top + gameTitle.getGlobalBounds().height + 8);

    URLButton.setFontSize(24U);
    URLButton.setText("www.zencat.pairg.ufrn.br");
    URLButton.setSizeToText();
    URLButton.setTextColor(sf::Color::Black);
    URLButton.setPosition(SCREEN_LENGHT/2 - URLButton.getSize().x/2, version.getPosition().y + version.getGlobalBounds().height + 20);

    PAIRGButton.setTexture(texturesDAO.getTexture("pairg"));
    PAIRGButton.setPosition(static_cast<int>(SCREEN_LENGHT*0.32 - PAIRGButton.getSize().x/2), SCREEN_HEIGHT*0.64);

    UFRNButton.setTexture(texturesDAO.getTexture("ufrn"));
    UFRNButton.setPosition(PAIRGButton.getPosition().x + PAIRGButton.getSize().x + 64,
                           PAIRGButton.getPosition().y + static_cast<int>(PAIRGButton.getSize().y/2 - UFRNButton.getSize().y/2));

    checkVersionButton.setTexture (texturesDAO.getTexture("button_update"));
    checkVersionButton.setHoverTexture(texturesDAO.getTexture("button_update_hover"));
    checkVersionButton.setPosition(SCREEN_LENGHT*0.98 - checkVersionButton.getSize().x, EXIT_POSITION_Y);
}

About::~About(){

}

void About::draw(const float dt){
    app->window.draw(background);
    app->window.draw(title);

    //app->window.draw(cat);
    app->window.draw(gameTitle);
    app->window.draw(backButton);
    app->window.draw(icon);
    app->window.draw(URLButton);
    app->window.draw(PAIRGButton);
    app->window.draw(UFRNButton);
    app->window.draw(checkVersionButton);
    app->window.draw(version);
}

void About::update(const float dt){

}

void About::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(backButton.clicked(mouse.x, mouse.y)){
                app->popState();
            }

            if(UFRNButton.clicked(mouse.x, mouse.y)){
                ShellExecute(0, 0, "http://www.UFRN.br", 0, 0 , SW_SHOW );
            }

            if(PAIRGButton.clicked(mouse.x, mouse.y)){
                ShellExecute(0, 0, "http://www.PAIRG.UFRN.br", 0, 0 , SW_SHOW );
            }

            if(URLButton.clicked(mouse.x, mouse.y)){
                ShellExecute(0, 0, "http://www.zencat.pairg.ufrn.br", 0, 0 , SW_SHOW );
            }

            if(checkVersionButton.clicked(mouse.x, mouse.y)){
                sf::Http http;
                http.setHost("http://server09.pairg.ufrn.br");

                sf::Http::Request request("/zencat.ver");

                std::cout << "Checking game version...\n" << std::endl;
                app->setGlobalMessage("Checking game version...");

                sf::Http::Response response = http.sendRequest(request);

                sf::Http::Response::Status status = response.getStatus();

                if (status == sf::Http::Response::Ok) {

                    std::string serverVersion = response.getBody();

                    std::cout << "Server version: " << serverVersion << std::endl;

                    if(compareServerVersion(serverVersion)){
                        std::cout << "You have the latest version of the game.\n" << std::endl;
                        app->setGlobalMessage("You have the latest version of the game.");
                    }
                    else{
                        std::cout << "A new version of the game is available.\n" << std::endl;
                        app->setGlobalMessage("A new version of the game is available.");
                    }
                }
                else{
                    std::cout << "Error " << status << std::endl;
                    app->setGlobalMessage("Error " + Utility::toString(status) + "\n");
                }


            }

            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(UFRNButton.contains(mouse.x, mouse.y) || PAIRGButton.contains(mouse.x, mouse.y) || URLButton.contains(mouse.x, mouse.y)){
                SystemCursor cursor(SystemCursor::HAND);
                cursor.applyCursor(app->window.getSystemHandle());
            }
            else{
                SystemCursor cursor(SystemCursor::NORMAL);
                cursor.applyCursor(app->window.getSystemHandle());
            }

            checkVersionButton.hover(mouse.x, mouse.y);
            backButton.hover(mouse.x, mouse.y);

            break;
        }


    }
}


/**
Compares the local game version to the server game version.
The line breaks and white spaces are removed to make the task easier.
Returns 'true' if the versions are the same, 'false' otherwise.
**/
bool About::compareServerVersion(std::string& serverVersion){

    std::string processedVersionString = "";

    int n = serverVersion.size();

    for(int i = 0; i < n; i++){
        if(serverVersion.at(i) != '\n' and serverVersion.at(i) != ' '){
            processedVersionString.push_back(serverVersion.at(i));
        }
    }

    return (processedVersionString == versionString);
}
