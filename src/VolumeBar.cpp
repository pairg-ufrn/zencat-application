#include "VolumeBar.hpp"

VolumeBar::VolumeBar(){
    frame.setTexture(texturesDAO.getTexture("sound_frame"));
    bar.setTexture(texturesDAO.getTexture("sound_bar"));

    soundIcon.setTexture(texturesDAO.getTexture("sound_full"));
    b_size = frame.getTexture()->getSize();
}

VolumeBar::~VolumeBar(){
    //dtor
}

void VolumeBar::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(frame);
    target.draw(bar);
    target.draw(soundIcon);
}

void VolumeBar::handleEvent(sf::Event& event){
    switch(event.type){

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            //sf::Vector2f mouse_world = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            sf::FloatRect body(position.x, position.y, b_size.x, b_size.y);
            if(body.contains(x, y)){
                float percentage = (x - position.x)/b_size.x;
                setVolume(percentage*100);
            }

        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            break;
        }
    }
}

void VolumeBar::setPosition(sf::Vector2f pos){
    position = pos;
    frame.setPosition(pos);
    bar.setPosition(pos.x + frame.getTexture()->getSize().x/2 - bar.getTexture()->getSize().x/2 + 1,
                    pos.y + frame.getTexture()->getSize().y/2 - bar.getTexture()->getSize().y/2);
    soundIcon.setPosition(bar.getPosition() + sf::Vector2f(bar.getGlobalBounds().width - soundIcon.getTexture()->getSize().x/2, -2));
}

void VolumeBar::setVolume(unsigned int vol){
    volume = vol;
    bar.setTextureRect(sf::IntRect(bar.getLocalBounds().left,
                                   bar.getLocalBounds().top,
                                   bar.getTexture()->getSize().x*(vol/100.f),
                                   bar.getLocalBounds().height));

    soundIcon.setPosition(bar.getPosition().x + bar.getTextureRect().width - soundIcon.getTexture()->getSize().x/2, bar.getPosition().y - 2);

    if(soundIcon.getPosition().x + soundIcon.getGlobalBounds().width > bar.getPosition().x + bar.getTexture()->getSize().x){
        soundIcon.setPosition(bar.getPosition().x + bar.getTexture()->getSize().x - soundIcon.getLocalBounds().width + 1, soundIcon.getPosition().y);
    }
    if(soundIcon.getPosition().x < bar.getPosition().x){
        soundIcon.setPosition(bar.getPosition().x - 1, soundIcon.getPosition().y);
    }

    if(vol <= 5){
        soundIcon.setTexture(texturesDAO.getTexture("sound_mute"));
    }
    else if(vol <= 50){
        soundIcon.setTexture(texturesDAO.getTexture("sound_half"));
    }
    else{
        soundIcon.setTexture(texturesDAO.getTexture("sound_full"));
    }

    soundPlayer.setGlobalVolume(volume);
}

unsigned int VolumeBar::getVolume(){
    return volume;
}


sf::Vector2u VolumeBar::getSize(){
    return b_size;
}

