#include "game.hpp"

Game::Game(int _difficulty_){

    GameState = GAMESTATE::COUNTDOWN;

    srand(time(NULL));

    alertText.setCharacterSize(30U);
    alertText.setColor(sf::Color::White);
    alertText.setFont(fontsDAO.getFont("agency_bold"));
    alertText.setPosition(SCREEN_LENGHT/2, SCREEN_HEIGHT*(4/5));
    alertText.setString("");

    gameView.setSize(SCREEN_LENGHT, SCREEN_HEIGHT);
    UIView.setSize(SCREEN_LENGHT, SCREEN_HEIGHT);
    UIView.setCenter(SCREEN_LENGHT/2, SCREEN_HEIGHT/2);

    topFrame.setTexture(texturesDAO.getTexture("top_frame"));
    meditationFrame.setTexture(texturesDAO.getTexture("meditation_frame"));
    meditationBar.setTexture(texturesDAO.getTexture("meditation_bar"));
    meditationLimit.setTexture(texturesDAO.getTexture("meditation_limit"));
    meditationMean.setTexture(texturesDAO.getTexture("meditation_mean"));

    meditationFrame.setPosition(SCREEN_LENGHT - meditationFrame.getLocalBounds().width - 4, 4);
    meditationBar.setPosition(meditationFrame.getPosition().x + meditationFrame.getTexture()->getSize().x/2 - meditationBar.getTexture()->getSize().x/2,
                              meditationFrame.getPosition().y + meditationFrame.getTexture()->getSize().y/2 - meditationBar.getTexture()->getSize().y/2);
    meditationBar.setScale(0, 1);

    meditationLimit.setPosition(meditationBar.getPosition() + sf::Vector2f(meditationBar.getLocalBounds().width*(minimumLimit/100.f) - meditationLimit.getLocalBounds().width/2, 0));

    meditationMean.setPosition (meditationBar.getPosition().x + meditationBar.getLocalBounds().width*(currentAverageMeditation/100.f) - meditationMean.getLocalBounds().width/2,
                                meditationBar.getPosition().y + meditationBar.getLocalBounds().height - meditationMean.getLocalBounds().height);

    heightMeter.setTexture(texturesDAO.getTexture("height_meter_0"));

    gameClockAnimation = new ClockAnimation(SCREEN_LENGHT/2 - texturesDAO.getTexture("clock_1").getSize().x/2, 1, sf::milliseconds(120000));

    heightMeter.setPosition(SCREEN_LENGHT*0.28, 4);

    animations.push_back(gameClockAnimation);

    gameClockText.setCharacterSize(40U);
    gameClockText.setColor(sf::Color::Black);
    gameClockText.setFont(fontsDAO.getFont("agency_bold"));
    gameClockText.setPosition(SCREEN_LENGHT*0.75, SCREEN_HEIGHT*(2/5));
    gameClockText.setString("");

    counterText.setCharacterSize(30U);
    counterText.setColor(sf::Color::Black);
    counterText.setFont(fontsDAO.getFont("agency_bold"));
    counterText.setPosition(SCREEN_LENGHT/2 - 6, SCREEN_HEIGHT/2 - 16);
    counterText.setString("");

    counterSprite.setTexture(texturesDAO.getTexture("counter_circle"));
    counterSprite.setPosition(SCREEN_LENGHT/2 - counterSprite.getTexture()->getSize().x/2,
                              SCREEN_HEIGHT/2 - counterSprite.getTexture()->getSize().y/2);

    initialHeight = static_cast<int>(-SCREEN_HEIGHT*0.3);

    cat.setTexture(texturesDAO.getTexture("cat"));
    cat.setOrigin(cat.getGlobalBounds().width/2, cat.getGlobalBounds().height/2);
    cat.setPosition( static_cast<int>(SCREEN_LENGHT/2),
                     initialHeight);
    pillow.setTexture(texturesDAO.getTexture("pillow"));
    pillow.setOrigin(pillow.getGlobalBounds().width/2, pillow.getGlobalBounds().height/2);
    pillow.setPosition(SCREEN_LENGHT/2,
                       cat.getPosition().y + cat.getGlobalBounds().height/2);

    trees.setTexture(texturesDAO.getTexture("null"));

    difficulty = _difficulty_;

    minimumLimit = baseLimit + 5*difficulty;

    switch(difficulty){
        case 0:
            background.setTexture(texturesDAO.getTexture("background_easy"));
        break;

        case 1:
            background.setTexture(texturesDAO.getTexture("background_medium"));
            animations.push_back(new WaterfallAnimation(SCREEN_LENGHT*0.244 - 1, -SCREEN_HEIGHT*1.455 - 72));
            soundPlayer.play("waterfall", true);
            soundPlayer.setMinimumDistance("waterfall", 5);
            soundPlayer.setAttenuation("waterfall", 16);
            soundPlayer.setPosition("waterfall", SCREEN_LENGHT/2, -SCREEN_HEIGHT*0.3);
            trees.setTexture(texturesDAO.getTexture("trees"), true);
            trees.setPosition(SCREEN_LENGHT - trees.getLocalBounds().width, -485);
        break;

        case 2:
            background.setTexture(texturesDAO.getTexture("background_hard"));
            animations.push_back(new IncenseAnimation(pillow.getPosition().x + pillow.getLocalBounds().width/2,
                                                      pillow.getPosition().y - pillow.getLocalBounds().height));
        break;
    }

    background.setOrigin(sf::Vector2f(0, background.getGlobalBounds().height));
    background.setPosition(0, 0);

    maximumHeight = background.getGlobalBounds().height - SCREEN_HEIGHT*0.5;

    //blinkSound.setBuffer(soundsDAO.getSound("blink"));

   // app->videoHandler.fadeIn();

    cout << "Generating clouds..." << endl;
    generateClouds();
    applyPenalty = false;

    manualGame = Utility::StringToNumber(configurationDAO.getConfiguration("ManualGame"));

    /* Confirm exit */

    confirmExitFrame.setTexture(texturesDAO.getTexture("frame_description"));

    confirmExitFrame.setPosition(SCREEN_LENGHT/2 - confirmExitFrame.getLocalBounds().width/2,
                                 SCREEN_HEIGHT/2 - confirmExitFrame.getLocalBounds().height/2);

    confirmExitText.setCharacterSize(30U);
    confirmExitText.setColor(sf::Color::Black);
    confirmExitText.setFont(fontsDAO.getFont("agency_bold"));
    confirmExitText.setString(translation(L"Do you really want to quit the game?"));
    confirmExitText.setPosition(SCREEN_LENGHT/2 - confirmExitText.getLocalBounds().width/2,
                                confirmExitFrame.getPosition().y + confirmExitFrame.getLocalBounds().height*0.2);

    confirmExitOptions.setCharacterSize(26U);
    confirmExitOptions.setColor(sf::Color::Black);
    confirmExitOptions.setFont(fontsDAO.getFont("agency_bold"));
    confirmExitOptions.setString("[ESC] " + translation(L"Yes") + ".\n[SPACE] " + translation(L"No") + ".");
    confirmExitOptions.setPosition(SCREEN_LENGHT/2 - confirmExitOptions.getLocalBounds().width/2,
                                   confirmExitFrame.getPosition().y + confirmExitFrame.getLocalBounds().height/2);


    /* Paused state*/

    pauseBackground.setTexture(texturesDAO.getTexture("background_dark"));
    pauseFrame.setTexture(texturesDAO.getTexture("frame_description"));

    pauseFrame.setPosition(SCREEN_LENGHT/2 - pauseFrame.getLocalBounds().width/2,
                           SCREEN_HEIGHT/2 - pauseFrame.getLocalBounds().height/2);

    pauseTitle.setCharacterSize(30U);
    pauseTitle.setColor(sf::Color::Black);
    pauseTitle.setFont(fontsDAO.getFont("agency_bold"));
    pauseTitle.setString("Paused");
    pauseTitle.setPosition(SCREEN_LENGHT/2 - pauseTitle.getLocalBounds().width/2, pauseFrame.getPosition().y + pauseFrame.getLocalBounds().height*0.2);

    pauseText.setCharacterSize(26U);
    pauseText.setColor(sf::Color::Black);
    pauseText.setFont(fontsDAO.getFont("agency_bold"));
    pauseText.setString(translation(L"Press SPACE to resume."));
    pauseText.setPosition(SCREEN_LENGHT/2 - pauseText.getLocalBounds().width/2, pauseFrame.getPosition().y + pauseFrame.getLocalBounds().height/2);

    penaltyClock.restart();
    startTime.restart();

}


Game::~Game(){
    app->window.setView(sf::View(sf::FloatRect(0, 0, SCREEN_LENGHT, SCREEN_HEIGHT)));
}


void Game::draw(const float dt){

    drawGame();

    if(GameState == GAMESTATE::PAUSED){
        drawPause();
    }

    if(confirmExit){
        app->window.setView(UIView);
        app->window.draw(confirmExitFrame);
        app->window.draw(confirmExitText);
        app->window.draw(confirmExitOptions);
        app->window.setView(gameView);
    }

}


void Game::drawGame(){

    //The cast to int is necessary to avoid distortions due to float values of pixels;
    if(static_cast<int>(cat.getPosition().y) > -SCREEN_HEIGHT/2){
        gameView.setCenter(static_cast<int>(SCREEN_LENGHT/2), static_cast<int>(-SCREEN_HEIGHT/2));
    }
    else{
        gameView.setCenter(static_cast<int>(cat.getPosition().x), static_cast<int>(cat.getPosition().y));
    }

    app->window.setView(gameView);

    app->window.draw(background);
    app->window.draw(pillow);


    for(auto& animation : animations){
        app->window.draw(*animation);
    }

    app->window.draw(trees);

    app->window.draw(cat);

    //To draw the top frame, move the window to the default position (UIView has this info.), draw and then go back.
    app->window.setView(UIView);

    app->window.draw(meditationFrame);
    app->window.draw(meditationBar);
    app->window.draw(meditationLimit);
    app->window.draw(meditationMean);
    app->window.draw(*gameClockAnimation);
    app->window.draw(heightMeter);

    if(GameState == GAMESTATE::COUNTDOWN){
        app->window.draw(counterSprite);
        app->window.draw(counterText);
    }

    app->window.setView(gameView);
}


void Game::drawPause(){
    app->window.setView(UIView);

    app->window.draw(pauseBackground);
    app->window.draw(pauseFrame);

    app->window.draw(pauseTitle);
    app->window.draw(pauseText);

    app->window.setView(gameView);
}



void Game::update(const float dt){

    app->statusVerifier.enable();

    if(not mindwaveModule.isConnected() or mindwaveModule.getSignalStrength() > 60){
        if(not manualGame){
            pauseText.setString(translation(L"Connection lost. Please adjust your headset."));
            pauseText.setPosition(SCREEN_LENGHT/2 - pauseText.getLocalBounds().width/2, pauseFrame.getPosition().y + pauseFrame.getLocalBounds().height/2);
            GameState = GAMESTATE::PAUSED;
            connectionLost = true;
        }

    }
    else{
        connectionLost = false;
    }

    switch(GameState){

        case GAMESTATE::PLAYING:{
            updateGame(dt);

            if(createAnimationClock.getElapsedTime().asSeconds() > 4){
                createAnimations();
                createAnimationClock.restart();
            }

            updateAnimations(dt);
        } break;

        case GAMESTATE::COUNTDOWN:{
            counterText.setString(Utility::toString(3 - static_cast<int>( counterClock.getElapsedTime().asSeconds() )));
            if(counterClock.getElapsedTime().asSeconds() > 3){
                GameState = GAMESTATE::PLAYING;
                gameClockAnimation->startClock();
                gameClock.restart();
            }
        }

        case GAMESTATE::PAUSED : {
            if(pauseClock.getElapsedTime().asSeconds() > 1){
                pausedTime++;
                pauseClock.restart();
            }
            if(not connectionLost){
                pauseText.setString(translation(L"Press SPACE to resume."));
                pauseText.setPosition(SCREEN_LENGHT/2 - pauseText.getLocalBounds().width/2,
                                      pauseFrame.getPosition().y + pauseFrame.getLocalBounds().height/2);
            }
        }

    }

}

void Game::generateClouds(){

    int bgSize = background.getTexture()->getSize().y;
    int cloudHeight = static_cast<int>(bgSize*0.75);
    int initialHeight;

    if(difficulty == 0){
        initialHeight = static_cast<int>(bgSize*0.15);
    }
    else if(difficulty == 1){
        initialHeight = static_cast<int>(bgSize*0.30);
    }
    else{
        initialHeight = static_cast<int>(bgSize*0.30);
    }

    float chance;
    int qtd = 0;

    for(int i = initialHeight; i < cloudHeight; i++){
        chance = (rand()%1000)/1000.f;

        if(chance >= 0.990f){
            qtd++;
            float posX = (rand()%100 > 50)? 0 : SCREEN_LENGHT;
            int id = 1 + rand()%7;

            animations.push_back(new CloudAnimation(posX, -i, difficulty + 1, id));
        }

    }
    cout << "Generated " << qtd << " clouds." << endl;
}


void Game::createAnimations(){
    int pos = -cat.getPosition().y; //the cat goes UP in the Y axis, which, in SFML, decreases

    int bgSize = background.getTexture()->getSize().y;

    float posX = (rand()%100 > 50)? 0 : SCREEN_LENGHT;
    float posY = (-pos + SCREEN_HEIGHT/2) - rand()%(SCREEN_HEIGHT);

    if(pos < bgSize*0.25){
        switch(difficulty){
            case 0:{
                if(pos > bgSize*0.10){
                    animations.push_back(new BirdAnimation(posX, posY));
                }
                else{
                    animations.push_back(new LeafAnimation(posX, posY + SCREEN_HEIGHT*0.5));
                }
            }
            break;

            case 1:{

                int dir = rand()%100;

                animations.push_back(new MothAnimation(posX, posY));

                (dir > 50) ? dir = -1 : dir = 1;
                animations.push_back(new FishAnimation(SCREEN_LENGHT*0.56, -SCREEN_HEIGHT*0.58, dir));

            }
            break;

            case 2:{
                animations.push_back(new DragonFlyAnimation(posX, posY));
            }
            break;
        }

    }
    else if(pos < bgSize*0.50){

        switch(difficulty){
            case 0:{

                int starChance = rand()%1000;
                int airplaneChance = rand()%1000;

                if(starChance > airplaneChance){
                    animations.push_back(new StarAnimation(posX, posY));
                }
                else{
                    animations.push_back(new AirplaneAnimation(posX, posY));
                }
            }
            break;

            case 1:{
                int birdChance = rand()%1000;
                int airshipChance = rand()%1000;
                if(birdChance > airshipChance){
                    animations.push_back(new BirdAnimation(posX, posY));
                }
                else{
                    animations.push_back(new BalloonAnimation(posX, posY));
                }
            }
            break;

            case 2:{
                int starChance = rand()%1000;
                int fireflyChance = rand()%1000;
                if(starChance > fireflyChance){
                    animations.push_back(new StarAnimation(posX, posY));
                }
                else{
                    animations.push_back(new FireflyAnimation(posX, posY));
                }

            }
            break;
        }
    }
    else if(pos < bgSize*0.75){
        animations.push_back(new RocketAnimation(posX, posY));
    }
    else if(pos < bgSize*0.86){

        int cometChance = rand()%1000;
        int ovniChance = rand()%1000;

        if(cometChance > ovniChance){
            animations.push_back(new CometAnimation(posX, posY));
        }
        else{
            animations.push_back(new OVNIAnimation(posX, posY));
        }
    }
}


void Game::updateAnimations(float dt){
    std::vector<Animation*>::iterator it = animations.begin();

    while(it != animations.end()){
        if(!(*it)->stillExists()){
            delete (*it);
            it = animations.erase(it);
        }
        else{
            (*it)->update(dt);
            it++;
        }
    }
}


void Game::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

        case sf::Event::KeyPressed: {
            char input = static_cast<char>(event.text.unicode);
            switch(input){

                case sf::Keyboard::Up:
                    if(manualGame){
                        if(GameState == GAMESTATE::PLAYING){
                            for(int i = 0; i < LAST_VALUES; i++){
                                meditationValues.push_back(100);
                            }
                        }
                    }
                break;

                case sf::Keyboard::Down:
                    if(manualGame){
                        if(GameState == GAMESTATE::PLAYING){
                            for(int i = 0; i < LAST_VALUES; i++){
                                meditationValues.push_back(1);
                            }
                        }
                    }
                break;

                case sf::Keyboard::P:
                    if(GameState == GAMESTATE::PLAYING){
                        GameState = GAMESTATE::PAUSED;
                        paused = true;
                        soundPlayer.stopAll();
                    }
                break;

                case sf::Keyboard::Space:{
                    if(GameState == GAMESTATE::PAUSED){
                        if(not connectionLost){
                            GameState = GAMESTATE::PLAYING;
                            paused = false;
                        }
                    }
                    else if(confirmExit){
                        confirmExit = false;
                        paused = false;
                    }
                }
                break;

                case sf::Keyboard::Right:
                    if(manualGame){
                        if(GameState == GAMESTATE::PLAYING){
                            meditationValues.push_back(currentAverageMeditation + 5);
                        }
                    }
                break;

                case sf::Keyboard::Left:
                    if(manualGame){
                        if(GameState == GAMESTATE::PLAYING){
                            meditationValues.push_back(currentAverageMeditation - 5);
                        }
                    }
                break;

                case sf::Keyboard::End:
                    if(manualGame){
                        result = 1;
                        setGameover();
                    }
                break;

                case sf::Keyboard::PageUp:
                    if(manualGame){
                        cat.setPosition(cat.getPosition() + sf::Vector2f(0, -250));
                    }
                break;

                case sf::Keyboard::Escape:
                    if(confirmExit){
                        soundPlayer.stopAll();
                        app->statusVerifier.disable();
                        app->popState();
                    }
                    else{
                        confirmExit = true;
                        paused = true;
                    }

                break;
            }
       } break;

       case sf::Event::MouseMoved:{
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            //
            break;
       }

        case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            //
        } break;
    }

}



void Game::testGameover(){
    if(cat.getPosition().y - cat.getTexture()->getSize().y/2 <= -background.getGlobalBounds().height*(4.f/5) + SCREEN_HEIGHT*0.5){
        result = 1;
    }
    if(gameClock.getElapsedTime().asSeconds() - pausedTime >= 120 || cat.getPosition().y <= -maximumHeight){
        setGameover();
    }
    return;
}

void Game::setGameover(){
    cout << "Game Over" << endl;
    GameState = GAMESTATE::OVER;

    if(result){
        int unlocked = Utility::StringToNumber(configurationDAO.getConfiguration("Unlocked"));
        if(unlocked == difficulty and difficulty != static_cast<int>(DIFFICULTY::HARD)){
            configurationDAO.setUnlocked(difficulty + 1);
            unlockedNewDifficulty = true;
        }
    }

    int level = background.getGlobalBounds().height;

    if(bestHeight < level*(1.f/5) + SCREEN_HEIGHT/5){
        performance = 1;
    }
    else if(bestHeight < level*(2.f/5) + SCREEN_HEIGHT/5){
        performance = 2;
    }
    else if(bestHeight < level*(3.f/5) + SCREEN_HEIGHT/5){
        performance = 3;
    }
    else if(bestHeight < level*(4.f/5) + SCREEN_HEIGHT/5){
        performance = 4;
    }
    else{
        performance = 5;
    }

    if(bestHeight - getPlayerHeight() >= getMaximumHeight()*0.2){
        performance--;
    }

    soundPlayer.stopAll();

    app->window.setView(app->window.getDefaultView());

    app->window.setMouseCursorVisible(true);

    app->changeState(new GameOverScreen(this));
}


void Game::updateGame(const float dt){

    /***** Update headset values *****/
    if(mindwaveModule.isConnected()){
        meditation = mindwaveModule.getMeditation();
        attention = mindwaveModule.getAttention();
    }
    else{
        attention = 0;
        meditation = 0;
        meditationBar.setScale(0, 1);
    }

    if(meditation > 0){
        meditationBar.setScale(meditation/100.f, 1);
        meditationValues.push_back(meditation);
    }
    if(attention > 0){
        attentionValues.push_back(attention);
    }

    /***** Update mediation limits *****/
    int bgSize = background.getTexture()->getSize().y;
    int pos = cat.getPosition().y*(-1);

    if(pos < bgSize*(1.f/5)){
        minimumLimit = baseLimit + 5*difficulty; //40 45 50
    }
    else if(pos < bgSize*(2.f/5)){
        minimumLimit = baseLimit + 5*difficulty + (5 + difficulty); //45 51 57
    }
    else if(pos < bgSize*(3.f/5)){
        minimumLimit = baseLimit + 5*difficulty + (5 + difficulty) + (9 + difficulty); //54 61 68
    }
    else if(pos < bgSize*(4.f/5)){
        minimumLimit = baseLimit + 5*difficulty + (5 + difficulty) + (9 + difficulty) + (10 + difficulty); //64 72 80
    }
    else{
        minimumLimit = baseLimit + 5*difficulty + (5 + difficulty) + (9 + difficulty) + (10 + difficulty) + (11 + difficulty); //75 84 93
    }

    currentAverageMeditation = getAverageMeditation(LAST_VALUES);

    meditationLimit.setPosition(meditationBar.getPosition() + sf::Vector2f(meditationBar.getLocalBounds().width*(minimumLimit/100.f) - meditationLimit.getLocalBounds().width/2, 0));
    meditationMean.setPosition (meditationBar.getPosition().x + meditationBar.getLocalBounds().width*(currentAverageMeditation/100.f) - meditationMean.getLocalBounds().width/2,
                                meditationBar.getPosition().y + meditationBar.getLocalBounds().height - meditationMean.getLocalBounds().height);


    /***** Update cat position *****/

    if(penaltyClock.getElapsedTime().asMilliseconds() > 2000){
        applyPenalty = false;
    }

    if(currentAverageMeditation >= minimumLimit and not applyPenalty){

        cat.setPosition(cat.getPosition().x, static_cast<int>(cat.getPosition().y - 1));
        cat.setTexture(texturesDAO.getTexture("cat"));

        meditationMean.setTexture(texturesDAO.getTexture("meditation_mean_green"), true);

    }
    else if(currentAverageMeditation >= minimumLimit*0.80){
        if(not applyPenalty and getPlayerHeight() > -initialHeight){
            applyPenalty = true;
            penaltyClock.restart();
        }

        if(cat.getPosition().y < initialHeight){
            cat.setPosition(cat.getPosition().x, static_cast<int>(cat.getPosition().y + 0.5));
        }
        cat.setTexture(texturesDAO.getTexture("cat_2"));
        meditationMean.setTexture(texturesDAO.getTexture("meditation_mean"), true);
    }
    else{
        if(not applyPenalty and getPlayerHeight() > -initialHeight){
            applyPenalty = true;
            penaltyClock.restart();
        }

        if(cat.getPosition().y < initialHeight){
            cat.setPosition(cat.getPosition().x, static_cast<int>(cat.getPosition().y + 1));
        }
        cat.setTexture(texturesDAO.getTexture("cat_3"));
        meditationMean.setTexture(texturesDAO.getTexture("meditation_mean_red"), true);
    }

    if(getPlayerHeight() > bestHeight){
        bestHeight = getPlayerHeight();
    }

    /***** Update height meter *****/

    if(getPlayerHeight() > maximumHeight*(4.f/5)){
        heightMeter.setTexture(texturesDAO.getTexture("height_meter_5"), true);
    }
    else if(getPlayerHeight() > maximumHeight*(3.f/5)){
        heightMeter.setTexture(texturesDAO.getTexture("height_meter_4"), true);
    }
    else if(getPlayerHeight() > maximumHeight*(2.f/5)){
        heightMeter.setTexture(texturesDAO.getTexture("height_meter_3"), true);
    }
    else if(getPlayerHeight() > maximumHeight*(1.f/5)){
        heightMeter.setTexture(texturesDAO.getTexture("height_meter_2"), true);
    }
    else if(getPlayerHeight() > 0){
        heightMeter.setTexture(texturesDAO.getTexture("height_meter_1"), true);
    }
    else{
        heightMeter.setTexture(texturesDAO.getTexture("height_meter_0"), true);
    }

    /***** Update waterfall sound for the medium scenario *****/
    if(difficulty == 1){
        //sf::Listener::setPosition(cat.getPosition().x, cat.getPosition().y, 0);

        if(abs(cat.getPosition().y) - SCREEN_HEIGHT*0.3 <= 1600){
            soundPlayer.setVolume("waterfall", ((1600 - (abs(cat.getPosition().y) - SCREEN_HEIGHT*0.3))/1600.f)*100.f);
        }
        else{
            soundPlayer.setVolume("waterfall", 0);
        }
    }

    testGameover();
}




/**

Returns the average meditation of the user. If lastValues is greater than zero and
less or equal than the vector's size, returns the average of the last N values.

**/
float Game::getAverageMeditation(const int& lastValues){
    int n       = meditationValues.size(),
        k       = 0,
        bottom  = 0,
        pos;

    float average = 0.f;

    if(n > 0){

        pos = n - 1;

        if(lastValues > 0 and lastValues <= n){
            bottom = n - lastValues;
        }

        for(int i = pos; i >= bottom; i--){
            average += meditationValues.at(i);
            k++;
        }

        average /= k;
    }

    return roundf(average*100)/100; //Only two decimal places, rounded to the nearest.
}

int Game::getDifficulty(){
    return difficulty;
}

int Game::getResult(){
    return result;
}

int Game::getPerformance(){
    return performance;
}

int Game::getBestHeight(){
    return bestHeight;
}

int Game::getPlayerHeight(){
    return -cat.getPosition().y + initialHeight;
}

int Game::getMaximumHeight(){
    return maximumHeight;
}

float Game::getHeightPercentage(){

    float total  = maximumHeight - SCREEN_HEIGHT*0.3;

    float result = bestHeight/total;

    return roundf(result*10000)/100.f; //Two decimal places

}

std::vector<int> Game::getAttentionValues(){
    return attentionValues;
}

std::vector<int> Game::getMeditationValues(){
    return meditationValues;
}

int Game::getUnlockedNewDifficulty(){
    return unlockedNewDifficulty;
}

void Game::setUnlockedNewDifficulty(const bool& unlocked){
    unlockedNewDifficulty = unlocked;
}

int Game::getDuration(){
    return static_cast<int>(startTime.getElapsedTime().asSeconds());
}

GAMESTATE Game::getGameState(){
    return GameState;
}


void Game::setApp(Application* app){
    this->app = app;
}
