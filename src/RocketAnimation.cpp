#include "RocketAnimation.hpp"

RocketAnimation::RocketAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("rocket_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("rocket_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("rocket_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("rocket_4")));

    setPosition(sf::Vector2f(x , y));

    paused = false;
    if(getPosition().x < SCREEN_LENGHT/2){
        //setPosition(getPosition() + sf::Vector2f(-sf::Sprite(texturesDAO.getTexture("rocket_1")).getTexture()->getSize().x, 0));
        direction = 1;
        flipX();
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0));

    }

    name = "rocket";

    soundPlayer.play("spacecraft");
}

RocketAnimation::~RocketAnimation(){
    //dtor
}

void RocketAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction*2, 0));
}

bool RocketAnimation::stillExists(){
    if(getPosition().x <= SCREEN_LENGHT*1.5 and getPosition().x >= (-1)*SCREEN_LENGHT*0.5){
        return true;
    }
    return false;
}
