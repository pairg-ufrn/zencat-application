#include "TextureRenderer.hpp"

TextureRenderer::TextureRenderer(){
    //ctor
}

TextureRenderer::~TextureRenderer(){
    //dtor
}

void TextureRenderer::rescaleTexture(sf::Texture* texture, const unsigned int& size_x, const unsigned int& size_y){
    sf::RenderTexture scaledTexture;
    sf::Sprite originalTexture(*texture);



    //scaledTexture.create(size_x, size_y, false);

    scaledTexture.create(texture->getSize().x, texture->getSize().y, false);
    scaledTexture.setSmooth(true);
    scaledTexture.clear(sf::Color::Transparent);
    scaledTexture.draw(originalTexture);
    scaledTexture.display();

    sf::Sprite temp(scaledTexture.getTexture());
    temp.scale(450.f/512.f, 675.f/768.f);

    *texture = *temp.getTexture();
    //texture->setSmooth(true);
}

void TextureRenderer::renderTexture(sf::Texture* texture, sf::RenderTexture renderedTexture){
}
