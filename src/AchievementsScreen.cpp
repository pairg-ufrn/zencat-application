#include "AchievementsScreen.hpp"

AchievementsScreen::AchievementsScreen(Application* app) : returnButton(EXIT_POSITION_X, EXIT_POSITION_Y, 48, 48, ""){
    this->app = app;

    app->statusVerifier.disable();

    notification.setTexture(texturesDAO.getTexture("notification"));

    returnButton.setTexture(texturesDAO.getTexture("button_back"));
    returnButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));

    background.setTexture(texturesDAO.getTexture("background_paws"));
    darkBackground.setTexture(texturesDAO.getTexture("background_dark"));
    descriptionFrame.setTexture(texturesDAO.getTexture("frame_description"));
    frame.setTexture(texturesDAO.getTexture("frame_options"));

    frame.setPosition(static_cast<int>(SCREEN_LENGHT/2 - frame.getTexture()->getSize().x/2),
                      static_cast<int>(SCREEN_HEIGHT/2 - frame.getTexture()->getSize().y/2));

    descriptionFrame.setPosition(static_cast<int>(SCREEN_LENGHT/2 - descriptionFrame.getTexture()->getSize().x/2),
                                 static_cast<int>(SCREEN_HEIGHT/2 - descriptionFrame.getTexture()->getSize().y/2));

    title.setCharacterSize(64U);
    title.setString(translation(L"Achievements"));
    title.setColor(sf::Color::Black);
    title.setFont(fontsDAO.getFont("life_is_goofy"));
    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, -16);

    achievementTitle.setCharacterSize(32U);
    achievementTitle.setString("");
    achievementTitle.setColor(sf::Color::Black);
    achievementTitle.setFont(fontsDAO.getFont("agency_bold"));
    achievementTitle.setPosition(SCREEN_LENGHT/2 - achievementTitle.getLocalBounds().width/2,
                                 static_cast<int>(descriptionFrame.getPosition().y + 8));

    achievementDescription.setCharacterSize(24U);
    achievementDescription.setString("");
    achievementDescription.setColor(sf::Color::Black);
    achievementDescription.setFont(fontsDAO.getFont("agency_bold"));
    achievementDescription.setPosition( SCREEN_LENGHT/2 - achievementDescription.getLocalBounds().width/2,
                                        static_cast<int>(descriptionFrame.getPosition().y*1.2));

    achievementsManager.reloadAchievements(); //In case the user has changed the language
    achievements = achievementsManager.getAchievements();

    int posX = frame.getPosition().x + SCREEN_LENGHT*0.06,
        posY = SCREEN_HEIGHT*0.19;
    int i = 0, j = 0;

    for(auto& achiev : achievements){
        Button newAchiev;

        newAchiev.setPosition(posX + i, posY + j);

        if(achiev.obtained){
            newAchiev.setTexture(texturesDAO.getTexture(achiev.textureName));
            newAchiev.setHoverTexture(texturesDAO.getTexture(achiev.textureName + "_hover"));
        }
        else{
            newAchiev.setTexture(texturesDAO.getTexture("achievement_slot"));
            newAchiev.setHoverTexture(texturesDAO.getTexture("achievement_slot_hover"));
        }

        buttons.push_back(newAchiev);

        i += SCREEN_LENGHT*0.205;
        if(i >= SCREEN_LENGHT*0.6){
            i = 0;
            j += SCREEN_HEIGHT*0.22;
        }
    }
}

AchievementsScreen::~AchievementsScreen(){
    //dtor
}


void AchievementsScreen::draw(const float dt){
    app->window.draw(background);

    app->window.draw(frame);

    app->window.draw(title);

    app->window.draw(returnButton);

    for(auto& button : buttons){
        app->window.draw(button);
    }

    if(showDescription){
        app->window.draw(darkBackground);
        app->window.draw(descriptionFrame);
        app->window.draw(achievementTitle);
        app->window.draw(achievementDescription);
    }

}

void AchievementsScreen::update(const float dt){

}

void AchievementsScreen::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse_world = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(showDescription){
                sf::FloatRect body(descriptionFrame.getPosition().x, descriptionFrame.getPosition().y,
                                   descriptionFrame.getLocalBounds().width, descriptionFrame.getLocalBounds().height);
                if(not body.contains(mouse_world.x, mouse_world.y)){ //clicked outside of the frame
                    showDescription = false;
                }
            }

            else{
                if(returnButton.clicked(mouse_world.x, mouse_world.y)){
                    app->popState();
                }

                int i, n = buttons.size();
                for(i = 0; i < n; i++){
                    if(buttons.at(i).clicked(mouse_world.x, mouse_world.y)){
                        achievements.at(i).recentlyObtained = false;
                        openAchievementDescription(achievements.at(i));
                    }
                }
            }

            break;
        }

        case sf::Event::MouseMoved: {

            if(not showDescription){
                int x = event.mouseMove.x;
                int y = event.mouseMove.y;

                sf::Vector2f mouse_world = app->window.mapPixelToCoords(sf::Vector2i(x, y));

                returnButton.hover(mouse_world.x, mouse_world.y);

                for(auto& button : buttons){
                    button.hover(mouse_world.x, mouse_world.y);
                }
            }

            break;
        }

        case sf::Event::KeyPressed: {
            if(event.key.code == sf::Keyboard::Escape){
                showDescription = false;
            }
            break;
        }
    }
}

void AchievementsScreen::openAchievementDescription(const Achievement& achiev){
    achievementTitle.setString(achiev.title);
    achievementDescription.setString(achiev.description);

    achievementTitle.setPosition(SCREEN_LENGHT/2 - achievementTitle.getLocalBounds().width/2,
                                 static_cast<int>(descriptionFrame.getPosition().y*1.2));

    //Prevents the text from leaving the frame area
    if(achievementDescription.getLocalBounds().width > descriptionFrame.getLocalBounds().width - 4){
        std::string currentString = achievementDescription.getString();
        int stringSize = currentString.size();
        int position = stringSize/2;

        for(int i = position; i < stringSize; i++){
            if(currentString.at(i) == ' '){
                currentString.at(i) = '\n';
                break;
            }
        }

        achievementDescription.setString(currentString);
    }

    achievementDescription.setPosition( SCREEN_LENGHT/2 - achievementDescription.getLocalBounds().width/2,
                                        static_cast<int>(descriptionFrame.getPosition().y*1.45));

    showDescription = true;
}

