#include "WaterfallAnimation.hpp"

WaterfallAnimation::WaterfallAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("waterfall_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("waterfall_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("waterfall_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("waterfall_4")));

    updateTime = sf::milliseconds(100);

    setPosition(sf::Vector2f(x, y));

    name = "waterfall";

    paused = false;

}

WaterfallAnimation::~WaterfallAnimation(){
    //dtor
}
