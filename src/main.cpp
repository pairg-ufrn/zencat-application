#include <iostream>

#include "application.hpp"
#include "mainMenu.hpp"
#include "LoadingScreen.hpp"
#include "SplashScreen.hpp"

#ifdef _WIN32
#include <windows.h>
#endif // _WIN32

using namespace std;

int main(){

    #ifdef _WIN32
        if(not Utility::StringToNumber(configurationDAO.getConfiguration("Console"))){
            FreeConsole();
        }
    #endif // _WIN32

    LoadingScreen loadingScreen;

    fontsDAO.loadFonts();

    languageDAO.loadLanguage();

    soundsDAO.loadSoundBuffers();

    texturesDAO.loadTextures();

    loadingScreen.close();

    Application app;

    app.pushState(new MainMenu(&app));

    app.pushState(new SplashScreen(&app));

    app.mainLoop();

    return 0;
}
