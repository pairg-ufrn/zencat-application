#include "GameOverScreen.hpp"

GameOverScreen::GameOverScreen(Game* game){
    app = game->app;
    this->game = game;
    background.setTexture(texturesDAO.getTexture("background_paws"));

    gameOverText.setCharacterSize(30U);
    gameOverText.setColor(sf::Color::Black);
    gameOverText.setFont(fontsDAO.getFont("agency_bold"));
    gameOverText.setString(  translation(L"Average meditation") + ": "
                           + Utility::toString(game->getAverageMeditation(0)) + "\n"

                           + translation(L"Maximum height") + ": "
                           + Utility::toString(game->getHeightPercentage()));

    gameOverText.setPosition(SCREEN_LENGHT/2 - gameOverText.getGlobalBounds().width*0.5 + 16,
                             SCREEN_HEIGHT*0.65);

    performanceScoreFrame.setTexture(texturesDAO.getTexture("performance_awful_frame"));
    performanceScoreFrame.setPosition(SCREEN_LENGHT/2 - performanceScoreFrame.getTexture()->getSize().x/2,
                                      SCREEN_HEIGHT*0.08);

    performanceScoreFace.setTexture(texturesDAO.getTexture("performance_awful_face"));
    performanceScoreFace.setPosition(SCREEN_LENGHT/2 - performanceScoreFace.getTexture()->getSize().x/2,
                                     performanceScoreFrame.getPosition().y +
                                     static_cast<int>(performanceScoreFrame.getLocalBounds().height*0.5) -
                                     static_cast<int>(performanceScoreFace.getLocalBounds().height*0.55));

    performanceText.setCharacterSize(36U);
    performanceText.setColor(sf::Color::Black);
    performanceText.setFont(fontsDAO.getFont("agency_bold"));
    performanceText.setString(translation(L"Awful..."));
    performanceText.setPosition(SCREEN_LENGHT/2 - performanceText.getLocalBounds().width/2,
                                SCREEN_HEIGHT*0.55);

    achievementText.setCharacterSize(32U);
    achievementText.setColor(sf::Color(0xAE, 0x7D, 0x30));
    achievementText.setFont(fontsDAO.getFont("agency_bold"));
    if(achievementsManager.unlockedAchievement()){
        achievementText.setString(translation(L"You unlocked a new achievement!"));
        achievementText.setPosition(SCREEN_LENGHT/2 - achievementText.getLocalBounds().width/2,
                                gameOverText.getPosition().y + gameOverText.getLocalBounds().height + 4);
    }
    else{
        achievementText.setString("");
    }



    playAgainButton.setTexture(texturesDAO.getTexture("button_again"));
    playAgainButton.setHoverTexture(texturesDAO.getTexture("button_again_hover"));
    playAgainButton.setText("");
    playAgainButton.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - playAgainButton.getSize().x/2),
                               EXIT_POSITION_Y);

    returnButton.setTexture(texturesDAO.getTexture("button_back"));
    returnButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));
    returnButton.setText("");
    returnButton.setPosition(EXIT_POSITION_X, EXIT_POSITION_Y);

    updateScoreIndex = 1;

    updateScoreAnimation = true;
    updateScoreClock.restart();

    performance = game->getPerformance();

}

GameOverScreen::~GameOverScreen(){
    app->statusVerifier.enable();
}

void GameOverScreen::draw(const float dt){
    app->window.draw(background);

    app->window.draw(performanceScoreFrame);

    if(not updateScoreAnimation){
        app->window.draw(performanceText);
        app->window.draw(performanceScoreFace);
    }

    app->window.draw(returnButton);
    app->window.draw(playAgainButton);
    app->window.draw(gameOverText);
    app->window.draw(achievementText);
}

void GameOverScreen::update(const float dt){
    if(updateScoreAnimation){

        if(updateScoreClock.getElapsedTime().asMilliseconds() >= 750){

            updateScoreClock.restart();

            if(updateScoreIndex < performance){
                updateScoreIndex++;
                setPerformanceSprite(updateScoreIndex);
            }
            else{
                updateScoreAnimation = false;
            }

        }
    }
}

void GameOverScreen::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

        case sf::Event::KeyPressed: {
            char input = static_cast<char>(event.text.unicode);
            switch(input){

                case sf::Keyboard::Escape:
                    soundPlayer.stopAll();
                    app->statusVerifier.disable();
                    app->popState();
                break;
            }
       } break;

       case sf::Event::MouseMoved:{
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            returnButton.hover(mouse.x, mouse.y);

            playAgainButton.hover(mouse.x, mouse.y);

            break;
       }

        case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(returnButton.clicked(mouse.x, mouse.y)){
                app->changeState(new DifficultyMenu(app, game->getUnlockedNewDifficulty()));
            }

            if(playAgainButton.clicked(mouse.x, mouse.y)){
                int difficulty = game->getDifficulty();
                Game* newGame = new Game(difficulty);

                newGame->setUnlockedNewDifficulty(game->getUnlockedNewDifficulty());

                app->changeState(new Playing(app, newGame));
            }

        } break;
    }

}

void GameOverScreen::setPerformanceSprite(int _performance_){

    if(_performance_ == 1){
        performanceText.setString(translation(L"Awful..."));
        performanceScoreFrame.setTexture(texturesDAO.getTexture("performance_awful_frame"), true);
        performanceScoreFace.setTexture (texturesDAO.getTexture("performance_awful_face"), true);
    }
    else if(_performance_ == 2){
        performanceText.setString(translation(L"Bad"));
        performanceScoreFrame.setTexture(texturesDAO.getTexture("performance_bad_frame"), true);
        performanceScoreFace.setTexture (texturesDAO.getTexture("performance_bad_face"), true);
    }
    else if(_performance_ == 3){
        performanceText.setString(translation(L"Good"));
        performanceScoreFrame.setTexture(texturesDAO.getTexture("performance_good_frame"), true);
        performanceScoreFace.setTexture (texturesDAO.getTexture("performance_good_face"), true);
    }
    else if(_performance_ == 4){
        performanceText.setString(translation(L"Great!"));
        performanceScoreFrame.setTexture(texturesDAO.getTexture("performance_great_frame"), true);
        performanceScoreFace.setTexture (texturesDAO.getTexture("performance_great_face"), true);
    }
    else{
        performanceText.setString(translation(L"Perfect!!"));
        performanceScoreFrame.setTexture(texturesDAO.getTexture("performance_perfect_frame"), true);
        performanceScoreFace.setTexture (texturesDAO.getTexture("performance_perfect_face"), true);
    }

    performanceText.setPosition(SCREEN_LENGHT/2 - performanceText.getLocalBounds().width/2,
                                SCREEN_HEIGHT*0.55);
}
