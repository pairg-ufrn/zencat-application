#include "Utility.hpp"

using namespace std;


bool Utility::isEmpty(std::ifstream& pFile){
    return pFile.peek() == std::ifstream::traits_type::eof();
}

int Utility::StringToNumber ( const string &Text ){  //Text not by const reference so that the function can be used with a
	stringstream ss(Text);  //character array as argument
	int result;
	return ss >> result ? result : 0;
}

string Utility::toString(const double& number){
    std::ostringstream ss;
    ss << number;
    return ss.str();
}

string Utility::getDatestamp(std::string separator){

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));

    output << padIt(tm_mday) << separator << padIt(tm_mon + 1) << separator << now_in.tm_year + 1900;
    return output.str();
}

/**

Returns a std::string with the current time in the format "HOURS:MINUTES:SECONDS";
The time is acquired from the operational system's clock.

**/
string Utility::getTimestamp () {

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output <<  padIt(tm_hour) << ":" << padIt(tm_min) << ":" << padIt(tm_sec)
           << "";
    return output.str();
}

/**

Returns a std::string with the current time in the format "HOURS _separator_ MINUTES _separator_ SECONDS";
The time is acquired from the operational system's clock.
Passing ":" as argument has the same effect as calling the function without arguments.

**/
string Utility::getTimestamp (std::string separator) {

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output <<  padIt(tm_hour) << separator << padIt(tm_min) << separator << padIt(tm_sec)
           << "";
    return output.str();
}


string Utility::getDatestamp (bool date, bool hour) {

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output << "";
        if(date){
            output  <<  padIt(tm_mday)    << "/"
                    <<  padIt(tm_mon + 1) << "/"
                    <<  now_in.tm_year + 1900 << " ";
        }
        if(hour){
            output  <<  padIt(tm_hour) << ":" << padIt(tm_min) << ":" << padIt(tm_sec)
                    << "";
        }
    return output.str();
}


std::string Utility::toClock(double time){
    std::string clock = "";
    clock = clock + ((time/60 < 10) ? "0" : "") + Utility::toString(static_cast<int>(time/60)) + ":";
    time = static_cast<int>(time)%60;
    clock += ((time < 10) ? "0" : "") + Utility::toString(static_cast<int>(time));
    return clock;
}


string Utility::getYear(){
    using namespace std::chrono;
    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output << "" << now_in.tm_year + 1900;

    return output.str();
}

string Utility::getMonth(){

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output << "" << padIt(tm_mon + 1);

    return output.str();
}

string Utility::getDay(){

    ostringstream output;
    system_clock::time_point now = system_clock::now();
    time_t now_c = system_clock::to_time_t(now);
    tm now_in = *(localtime(&now_c));
    output << "" << padIt(tm_mday);

    return output.str();
}

bool Utility::isDigit(const char& digit){
    if(digit < '0' or digit > '9') return false;
    return true;
}


std::string Utility::encryptDecrypt(std::string phrase){
    char key[4] = {'G', 'A', 'M', 'V'};
    std::string output = phrase;
    int i, n = phrase.size();

    for (i = 0; i < n; i++)
        output[i] = phrase[i]^key[i%(sizeof(key)/sizeof(char))];

    return output;
}
