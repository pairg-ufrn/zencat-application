#include "ConfigMenu.hpp"

ConfigMenu::ConfigMenu(Application* app) {
    this->app = app;

    background.setTexture(texturesDAO.getTexture("background_difficulty"));

    frame.setTexture(texturesDAO.getTexture("frame_options"));
    frame.setPosition(SCREEN_LENGHT/2 - frame.getTexture()->getSize().x/2,
                      SCREEN_HEIGHT/2 - frame.getTexture()->getSize().y/2);

    title.setCharacterSize(32U);
    title.setString(translation(L"Options"));
    title.setColor(sf::Color::Black);
    title.setFont(fontsDAO.getFont("agency_bold"));
    title.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - title.getGlobalBounds().width/2),
                      static_cast<int>(SCREEN_HEIGHT*0.04));

    userTitle.setCharacterSize(30U);
    userTitle.setString(translation(L"Nickname"));
    userTitle.setColor(sf::Color::Black);
    userTitle.setFont(fontsDAO.getFont("agency_bold"));
    userTitle.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - userTitle.getGlobalBounds().width/2),
                          static_cast<int>(SCREEN_HEIGHT*0.2));

    languageTitle.setCharacterSize(30U);
    languageTitle.setString(translation(L"Language"));
    languageTitle.setColor(sf::Color::Black);
    languageTitle.setFont(fontsDAO.getFont("agency_bold"));
    languageTitle.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - languageTitle.getGlobalBounds().width/2),
                              static_cast<int>(SCREEN_HEIGHT*0.33));

    soundText.setCharacterSize(30U);
    soundText.setString(translation(L"Volume"));
    soundText.setColor(sf::Color::Black);
    soundText.setFont(fontsDAO.getFont("agency_bold"));
    soundText.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - soundText.getGlobalBounds().width/2),
                          static_cast<int>(SCREEN_HEIGHT*0.48));

    comPortTitle.setCharacterSize(30U);
    comPortTitle.setString(translation(L"COM Port"));
    comPortTitle.setColor(sf::Color::Black);
    comPortTitle.setFont(fontsDAO.getFont("agency_bold"));
    comPortTitle.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - comPortTitle.getGlobalBounds().width/2),
                              static_cast<int>(SCREEN_HEIGHT*0.64));

    comPortTextBox.setTextColor(sf::Color(0xAE, 0x7D, 0x30)); //brownish
    comPortTextBox.setSize(75, 32);
    comPortTextBox.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - comPortTextBox.getSize().x/2),
                                  comPortTitle.getPosition().y + comPortTitle.getLocalBounds().height + 4);
    comPortTextBox.setText(configurationDAO.getConfiguration("COMPort"));
    comPortTextBox.setTextAlignment(CENTER);
    comPortTextBox.setDigitsOnly(true);

    autoConnectButton.setFontSize(18U);
    autoConnectButton.setTextColor(sf::Color::Black);
    autoConnectButton.setText(translation(L"Auto Connect"));
    autoConnectButton.setFont(fontsDAO.getFont("agency_bold"));
    autoConnectButton.setTexture(texturesDAO.getTexture("button_horizontal"));
    autoConnectButton.setHoverTexture(texturesDAO.getTexture("button_horizontal_hover"));
    autoConnectButton.setPosition(SCREEN_LENGHT/2 - autoConnectButton.getSize().x/2,
                                  comPortTextBox.getPosition().y + comPortTextBox.getSize().y + 8);

    playerNameTextBox.setTextColor(sf::Color(0xAE, 0x7D, 0x30)); //brownish
    playerNameTextBox.setSize(150, 32);
    playerNameTextBox.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - playerNameTextBox.getSize().x/2),
                                  userTitle.getPosition().y + userTitle.getLocalBounds().height + 4);
    playerNameTextBox.setText(configurationDAO.getConfiguration("User"));
    playerNameTextBox.setTextAlignment(CENTER);

    volume = Utility::StringToNumber(configurationDAO.getConfiguration("Volume"));

    volumeBar.setPosition(sf::Vector2f(SCREEN_LENGHT/2 - volumeBar.getSize().x/2, soundText.getPosition().y + soundText.getGlobalBounds().height*2));

    volumeBar.setVolume(volume);

    languageList = languageDAO.getLanguageList();

    int numOfLanguages = languageList.size();

    languageText.setCharacterSize(30U);
    languageText.setColor(sf::Color(0xAE, 0x7D, 0x30));
    languageText.setFont(fontsDAO.getFont("agency_bold"));

    sf::Text maxTextSize = languageText; //Maximum language text size, to determine the position of the arrow buttons
    sf::Text auxSizeText = languageText;

    for(int i = 0; i < numOfLanguages; i++){

        auxSizeText.setString(languageList.at(i).languageName);

        if(auxSizeText.getLocalBounds().width > maxTextSize.getLocalBounds().width){
            maxTextSize.setString(languageList.at(i).languageName);
        }

        if(languageList.at(i).languageLocale == configurationDAO.getConfiguration("Language")){
            languageIndex = i;
            languageText.setString(languageList.at(i).languageName);
        }
    }

    languageText.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - languageText.getGlobalBounds().width/2),
                             languageTitle.getPosition().y + languageTitle.getLocalBounds().height*1.4);

    maxTextSize.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - maxTextSize.getGlobalBounds().width/2),
                             languageTitle.getPosition().y + languageTitle.getLocalBounds().height*1.4);

    nextLanguageButton.setTexture(texturesDAO.getTexture("button_right_small"));
    nextLanguageButton.setHoverTexture(texturesDAO.getTexture("button_right_small_hover"));
    nextLanguageButton.setPosition(maxTextSize.getPosition().x + maxTextSize.getLocalBounds().width + 32,
                                   languageText.getPosition().y + languageText.getLocalBounds().height/2 - nextLanguageButton.getSize().y/2 + 5);

    previousLanguageButton.setTexture(texturesDAO.getTexture("button_left_small"));
    previousLanguageButton.setHoverTexture(texturesDAO.getTexture("button_left_small_hover"));
    previousLanguageButton.setPosition(maxTextSize.getPosition().x - previousLanguageButton.getSize().x - 32,
                                       languageText.getPosition().y + languageText.getLocalBounds().height/2 - previousLanguageButton.getSize().y/2 + 5);

    saveButton.setTexture(texturesDAO.getTexture("button_save"));
    saveButton.setHoverTexture(texturesDAO.getTexture("button_save_hover"));
    saveButton.setPosition(SCREEN_LENGHT*0.98 - saveButton.getSize().x, EXIT_POSITION_Y);

    cancelButton.setTexture(texturesDAO.getTexture("button_back"));
    cancelButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));
    cancelButton.setPosition(EXIT_POSITION_X, EXIT_POSITION_Y);
}

ConfigMenu::~ConfigMenu(){
    //dtor
}

void ConfigMenu::draw(const float dt){

    app->window.draw(background);

    app->window.draw(frame);

    app->window.draw(title);

    app->window.draw(userTitle);
    app->window.draw(soundText);
    app->window.draw(volumeBar);
    app->window.draw(playerNameTextBox);

    app->window.draw(languageTitle);
    app->window.draw(languageText);

    app->window.draw(previousLanguageButton);
    app->window.draw(nextLanguageButton);

    app->window.draw(comPortTitle);
    app->window.draw(comPortTextBox);
    app->window.draw(autoConnectButton);

    app->window.draw(saveButton);
    app->window.draw(cancelButton);

    return;
}

void ConfigMenu::update(const float dt){

}

void ConfigMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            volumeBar.handleEvent(event);

            if(playerNameTextBox.clicked(mouse.x, mouse.y)){
                playerNameTextBox.select();
                comPortTextBox.deselect();
            }

            else if(comPortTextBox.clicked(mouse.x, mouse.y)){
                comPortTextBox.select();
                playerNameTextBox.deselect();
            }

            else if(autoConnectButton.clicked(mouse.x, mouse.y)){
                int newCOMPort = mindwaveModule.autoConnect();
                if(newCOMPort >= 0){
                    configurationDAO.setCOMPort(Utility::toString(newCOMPort));
                    comPortTextBox.setText(Utility::toString(newCOMPort));
                    app->setGlobalMessage(translation(L"USB device found on port COM") + Utility::toString(newCOMPort));
                }
                else{
                    app->setGlobalMessage(translation(L"No USB device found."));
                }


            }

            else if(previousLanguageButton.clicked(mouse.x, mouse.y)){
                languageIndex--;
                if(languageIndex < 0) languageIndex = languageList.size() - 1;

                languageText.setString(languageList.at(languageIndex).languageName);
                languageText.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - languageText.getGlobalBounds().width/2),
                                         languageTitle.getPosition().y + languageTitle.getLocalBounds().height*1.4);
            }

            else if(nextLanguageButton.clicked(mouse.x, mouse.y)){
                ++languageIndex %= languageList.size();

                languageText.setString(languageList.at(languageIndex).languageName);
                languageText.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - languageText.getGlobalBounds().width/2),
                                         languageTitle.getPosition().y + languageTitle.getLocalBounds().height*1.4);
            }

            else if(saveButton.clicked(mouse.x, mouse.y)){
                saveConfigurations();
                app->popState();
            }
            else if(cancelButton.clicked(mouse.x, mouse.y)){
                app->popState();
            }

            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            autoConnectButton.hover(mouse.x, mouse.y);
            previousLanguageButton.hover(mouse.x, mouse.y);
            nextLanguageButton.hover(mouse.x, mouse.y);
            saveButton.hover(mouse.x, mouse.y);
            cancelButton.hover(mouse.x, mouse.y);

            if(playerNameTextBox.contains(mouse.x, mouse.y)){
                SystemCursor cursor(SystemCursor::TEXT);
                cursor.applyCursor(app->window.getSystemHandle());
            }
            else if(comPortTextBox.contains(mouse.x, mouse.y)){
                SystemCursor cursor(SystemCursor::TEXT);
                cursor.applyCursor(app->window.getSystemHandle());
            }
            else{
                SystemCursor cursor(SystemCursor::NORMAL);
                cursor.applyCursor(app->window.getSystemHandle());
            }

            break;
        }

        case sf::Event::TextEntered: {
            if(playerNameTextBox.isSelected()){
                playerNameTextBox.processUserInput(event);
            }
            else if(comPortTextBox.isSelected()){
                comPortTextBox.processUserInput(event);
            }

        }
    }
}

void ConfigMenu::saveConfigurations(){
    if(playerNameTextBox.getString().size() > 0){
        playerNameTextBox.removeWhiteSpaces();
        configurationDAO.setUser(playerNameTextBox.getString());
    }

    configurationDAO.setAudio(volumeBar.getVolume());

    if(languageList.at(languageIndex).languageLocale != configurationDAO.getConfiguration("Language")){
        configurationDAO.setLanguage(languageList.at(languageIndex).languageLocale);
        languageDAO.loadLanguage();
    }

    if(comPortTextBox.getString().size() > 0){
        comPortTextBox.removeWhiteSpaces();
        configurationDAO.setCOMPort(comPortTextBox.getString());
    }


    cout << "Configurations saved" << endl;
}
