#include "TexturesDAO.hpp"

TexturesDAO::TexturesDAO(){
    //ctor
}

TexturesDAO::~TexturesDAO(){
    //dtor
}

TexturesDAO& TexturesDAO::getInstance(){
    static TexturesDAO instance;
    return instance;
}

std::string TexturesDAO::removeFilePath(const std::string& file){
    std::string result = "";

    unsigned int stringSize = file.size();
    unsigned int extensionPosition;
    unsigned int pathEndPosition;

    //Get the starting position of the file extension. Ex: in "file.png", the starting position is 4.
    for(extensionPosition = stringSize - 1; extensionPosition >= 0 and file.at(extensionPosition) != '.'; extensionPosition--);

    //Get the ending position of the file extension. Ex: in "images\file.png", the ending position is 7.
    for(pathEndPosition = extensionPosition; pathEndPosition >= 0 and file.at(pathEndPosition) != '\\'; pathEndPosition--);

    pathEndPosition++; //account for the counter-slash

    for(int i = pathEndPosition; i < extensionPosition; i++){
        result += file.at(i);
    }

    return result;
}


unsigned int TexturesDAO::findFiles(std::vector<std::string>& files, const std::string& directory){
    boost::filesystem::path dir(directory);

    if (!boost::filesystem::exists(dir))
        return 0;

    boost::filesystem::directory_iterator itEnd;
    for (boost::filesystem::directory_iterator it(dir); it != itEnd; it++){
        if (boost::filesystem::is_directory(it->status())){
            findFiles(files, it->path().string());
        }
        else if (boost::filesystem::is_regular(it->status())){
            files.push_back(it->path().string());
        }
        else; //Not a directory or regular file
    }
    return files.size();
}

void TexturesDAO::loadTextures(){

    std::cout << "Loading textures..." << std::endl;

    std::vector<std::string> files;

    findFiles(files, "media\\textures\\");

    unsigned int numberOfFiles = files.size();

    std::cout << "Found " << numberOfFiles << " textures." << std::endl;

    for(int i = 0; i < numberOfFiles; i++){
        loadTexture(removeFilePath(files.at(i)), files.at(i));
    }

    std::cout << "Finished loading textures." << std::endl;

    return;
}

sf::Texture& TexturesDAO::getTexture(const std::string& textureName){
    try{
        return this->textures.at(textureName);
    }
    catch(std::out_of_range& e){
        std::cout << "Texture " << textureName << " not found." << std::endl;
        return textures.at("null");
    }
}


void TexturesDAO::loadTexture(const std::string& name, const std::string& filename){
    sf::Texture texture;
    texture.loadFromFile(filename);

    this->textures[name] = texture;

    return;
}

