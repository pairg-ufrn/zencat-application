#include "FishAnimation.hpp"

FishAnimation::FishAnimation(const float& x, const float& y, const int& direction){
    addFrame(sf::Sprite(texturesDAO.getTexture("fish_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("fish_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("fish_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("fish_4")));
    addFrame(sf::Sprite(texturesDAO.getTexture("fish_5")));

    updateTime = sf::milliseconds(150);

    setPosition(sf::Vector2f(x, y));

    name = "fish";

    this->direction = direction;

    if(direction == 1){
        flipX();
    }

    paused = false;

    soundPlayer.play("splash_1");

    aliveTime.restart();
}

FishAnimation::~FishAnimation(){
    //dtor
}

void FishAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction*2, 0));
}

bool FishAnimation::stillExists(){
    if(aliveTime.getElapsedTime().asMilliseconds() < getDuration().asMilliseconds()){
        return true;
    }
    return false;
}
