#include "BalloonAnimation.hpp"

BalloonAnimation::BalloonAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("balloon")));

    setPosition(sf::Vector2f(x, y));

    name = "balloon";

    paused = false;

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        flipX();
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0));
    }

    //soundPlayer.play("bird1");
}

BalloonAnimation::~BalloonAnimation(){
    //dtor
}
