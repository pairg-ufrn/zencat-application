#include "MindwaveModule.hpp"

MindwaveModule::MindwaveModule(){
    connectionID = -1;
    lastAttentionValue = 0;
    lastMeditationValue = 0;
    lastAlpha1Value = 0;
    lastAlpha2Value = 0;
    lastBeta1Value = 0;
    lastBeta2Value = 0;
    lastGamma1Value = 0;
    lastGamma2Value = 0;
    lastRawValue = 0;
    lastThetaValue = 0;
    lastDeltaValue = 0;
    lastBlinkValue = -1;
}

MindwaveModule::~MindwaveModule(){
    disconnect();
}

MindwaveModule& MindwaveModule::getInstance(){
    static MindwaveModule instance;
    return instance;
}


int MindwaveModule::autoConnect(){
    int currentPort;
    int newConnectionID;
    int errorCode;
    char* comPortName;

    for(currentPort = 1; currentPort <= 12; currentPort++){
        newConnectionID = TG_GetNewConnectionId();

        comPortName = const_cast<char*>(("\\\\.\\COM" + toString(currentPort)).c_str());

        errorCode = TG_Connect( newConnectionID,
                                comPortName,
                                TG_BAUD_57600,
                                TG_STREAM_PACKETS );
        if(errorCode == 0){
            if(connectionID >= 0){
                TG_FreeConnection(connectionID);
            }

            connectionID = newConnectionID;
            enableBlinkDetection();
            enableAutoReading();
            comPort = comPortName;

            cout << "Found and connected USB Adapter on port: " << currentPort << endl;

            return currentPort;
        }
        else{
            TG_FreeConnection(newConnectionID);
        }
    }

    cout << "No USB Adapter was found." << endl;

    return -1;
}


int MindwaveModule::connect(const char* comPortName){

    int errCode = 0;

     //cout << "Connecting to port " << comPortName << "..." << endl;

    /* Get a connection ID handle to ThinkGear */
    connectionID = TG_GetNewConnectionId();

    if( connectionID < 0 ) {
       // cout << "ERROR: TG_GetNewConnectionId() returned " << connectionID << endl;
        return connectionID;
    }

    /* Set/open stream (raw bytes) log file for connection */
    errCode = TG_SetStreamLog( connectionID, "streamLog.txt" );
    if( errCode < 0 ) {
        cout << "ERROR: TG_SetStreamLog() returned " << errCode << endl;
        connectionID = errCode;
        return connectionID;
    }

    /* Set/open data (ThinkGear values) log file for connection */
    errCode = TG_SetDataLog( connectionID, "dataLog.txt" );
    if( errCode < 0 ) {
        cout << "ERROR: TG_SetDataLog() returned " << errCode << endl;
        connectionID = errCode;
        return connectionID;
    }

    /* Attempt to connect the connection ID handle to serial port comPortName */
    /* NOTE: On Windows, COM10 and higher must be preceded by \\.\, as in
     *       "\\\\.\\COM12" (must escape backslashes in strings).  COM9
     *       and lower do not require the \\.\, but are allowed to include
     *       them.  On Mac OS X, COM ports are named like
     *       "/dev/tty.MindSet-DevB-1".
     */
    errCode = TG_Connect( connectionID,
                          comPortName,
                          TG_BAUD_57600,
                          TG_STREAM_PACKETS );
    if( errCode < 0 ) {
        //cout << "ERROR: TG_Connect() returned " << errCode << endl;
        connectionID = errCode;
        return errCode;
    }
    if(errCode == 0){
        cout << "Connected: " << connectionID << endl;
        comPort = const_cast<char*>(comPortName);
        cout << comPort << endl;
        enableBlinkDetection();
        enableAutoReading();
        return connectionID;
    }

}

void MindwaveModule::enableAutoReading(){
    if(isConnected()){
        TG_EnableAutoRead(connectionID, true);
    }
}

int MindwaveModule::getConnectionID(){
    return connectionID;
}


bool MindwaveModule::isConnected(){
    if(connectionID < 0){
		return false;
	}
	else{
        return true;
	}
}

/**

Verifies whether the module is receiving updated packets from the headset.
As the TG_DATA_RAW data type is updated at 512Hz, its value is checked to verify the packets.

**/
bool MindwaveModule::isUpdating(){
    if(isConnected()){
        if(TG_GetValueStatus(connectionID, TG_DATA_RAW)){
            cout << (int)TG_GetValue(connectionID, TG_DATA_RAW) << endl;
            return true;
        }
    }

    return false;


}

int MindwaveModule::getAttention(){
    int att = (int)TG_GetValue(connectionID, TG_DATA_ATTENTION);

    if(lastAttentionValue != att){
        cout << "Attention: " << att << endl;
        lastAttentionValue = att;
        return att;
    }
    return 0;
}

int MindwaveModule::getLastAttentionValue(){
    return lastAttentionValue;
}

int MindwaveModule::getMeditation(){
    int med = (int)TG_GetValue(connectionID, TG_DATA_MEDITATION);

    if(lastMeditationValue != med){
        cout << "Meditation: " << med << endl;
        lastMeditationValue = med;
        return med;
    }

    return 0;
}


int MindwaveModule::getLastMeditationValue(){
    return lastMeditationValue;
}

int MindwaveModule::getRaw(){
    int raw = (int)TG_GetValue(connectionID, TG_DATA_RAW);

    if(lastRawValue != raw){
        lastRawValue = raw;
        return raw;
    }

    return 0;
}


int MindwaveModule::getAlpha1(){
    int alpha1 = (int)TG_GetValue(connectionID, TG_DATA_ALPHA1);

    if(lastAlpha1Value != alpha1){
        lastAlpha1Value = alpha1;
        return alpha1;
    }

    return 0;
}

int MindwaveModule::getAlpha2(){
    int alpha2 = (int)TG_GetValue(connectionID, TG_DATA_ALPHA2);

    if(lastAlpha2Value != alpha2){
        lastAlpha2Value = alpha2;
        return alpha2;
    }

    return 0;
}

int MindwaveModule::getBeta1(){
    int beta = (int)TG_GetValue(connectionID, TG_DATA_BETA1);

    if(lastBeta1Value != beta){
        lastBeta1Value = beta;
        return beta;
    }

    return 0;
}

int MindwaveModule::getBeta2(){
    int beta = (int)TG_GetValue(connectionID, TG_DATA_BETA2);

    if(lastBeta2Value != beta){
        lastBeta2Value = beta;
        return beta;
    }

    return 0;
}

int MindwaveModule::getGamma1(){
    int gamma = (int)TG_GetValue(connectionID, TG_DATA_GAMMA1);

    if(lastGamma1Value != gamma){
        lastGamma1Value = gamma;
        return gamma;
    }

    return 0;
}

int MindwaveModule::getGamma2(){
    int gamma = (int)TG_GetValue(connectionID, TG_DATA_GAMMA2);

    if(lastGamma2Value != gamma){
        lastGamma2Value = gamma;
        return gamma;
    }

    return 0;
}

int MindwaveModule::getDelta(){
    int delta = (int)TG_GetValue(connectionID, TG_DATA_DELTA);

    if(lastDeltaValue != delta){
        lastDeltaValue = delta;
        return delta;
    }

    return 0;
}


int MindwaveModule::getTheta(){
    int theta = (int)TG_GetValue(connectionID, TG_DATA_THETA);

    if(lastThetaValue != theta){
        lastThetaValue = theta;
        return theta;
    }

    return 0;
}

int MindwaveModule::getBatteryStrength(){

   return (int)TG_GetValue(connectionID, TG_DATA_BATTERY);

}

/**
The signal strength is a number that goes from 0 to 200, indicating the quality of the headset's signal.
However, this value is inverted, i.e., 0 (zero) means a high quality signal, while 200 means that there's no contact
between the electrode and the user's forehead. The higher the value, the poorest the signal.
**/
int MindwaveModule::getSignalStrength(){

    return (int)TG_GetValue(connectionID, TG_DATA_POOR_SIGNAL);

}

bool MindwaveModule::hasPoorSignal(){
    if(TG_GetValueStatus(connectionID, TG_DATA_POOR_SIGNAL) != 0){
        if(TG_GetValue(connectionID, TG_DATA_POOR_SIGNAL) > 180){
            return true;
        }
    }
    return false;
}

void MindwaveModule::enableBlinkDetection(){
    if(isConnected()){
        TG_EnableBlinkDetection(connectionID, true);
    }
}

int MindwaveModule::getBlinkStrength(){

    int blink = (int)TG_GetValue(connectionID, TG_DATA_BLINK_STRENGTH);

    if(lastBlinkValue != blink){

        cout << "Blink: " << blink << endl;

        lastBlinkValue = blink;
        return blink;
    }

    return 0;

}


void MindwaveModule::disconnect(){
    TG_FreeConnection(connectionID);
    connectionID = -1;
}


std::string MindwaveModule::toString(const int& number){
    std::ostringstream ss;
    ss << number;
    return ss.str();
}
