#include "StatusVerifier.hpp"

StatusVerifier::StatusVerifier(){
    frame.setTexture(texturesDAO.getTexture("frame_status"));
    frame.setPosition(4, 4);

    batteryStatus.setTexture(texturesDAO.getTexture("headset_battery_low"));
    headsetConnectionStatus.setTexture(texturesDAO.getTexture("headset_disconnected"));
    headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_none"));

    batteryStatus.setPosition     (frame.getPosition() +
                                   sf::Vector2f(frame.getLocalBounds().width*0.625 - batteryStatus.getTexture()->getSize().x,
                                                frame.getLocalBounds().height/2  - batteryStatus.getTexture()->getSize().y/2));

    headsetConnectionStatus.setPosition (frame.getPosition() +
                                         sf::Vector2f(frame.getTexture()->getSize().x*0.1,
                                                      frame.getTexture()->getSize().y/2 - headsetConnectionStatus.getTexture()->getSize().y/2));

    headsetSignalStatus.setPosition     (frame.getPosition() +
                                         sf::Vector2f(frame.getTexture()->getSize().x*0.9 - headsetSignalStatus.getTexture()->getSize().x,
                                                      frame.getTexture()->getSize().y/2   - headsetSignalStatus.getTexture()->getSize().y/2));
}

StatusVerifier::~StatusVerifier(){
    //dtor
}

void StatusVerifier::loop(){

}

void StatusVerifier::handleEvent(sf::Event& event){
    //When some of the status are clicked, tries to connect
}

void StatusVerifier::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    if(enabled){
        target.draw(frame);

        target.draw(batteryStatus);

        target.draw(headsetConnectionStatus);

        target.draw(headsetSignalStatus);
    }

}

void StatusVerifier::checkHeadsetStatus(){
    if(checkHeadsetConnectionClock.getElapsedTime().asSeconds() > 1){

        checkHeadsetConnectionClock.restart();

        if(!mindwaveModule.isConnected()){
            headsetConnectionStatus.setTexture(texturesDAO.getTexture("headset_disconnected"));
            headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_none"));
            headsetBatteryStatus.setTexture(texturesDAO.getTexture("headset_battery_low"));

            mindwaveModule.connect(("\\\\.\\COM" + configurationDAO.getConfiguration("COMPort")).c_str());

        }
        else{
            headsetConnectionStatus.setTexture(texturesDAO.getTexture("headset_connected"));

            if(mindwaveModule.isUpdating()){
                int signal = mindwaveModule.getSignalStrength();
                int battery = mindwaveModule.getBatteryStrength();

                if(signal >= 100){
                    headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_none"));
                }
                else if(signal >= 60){
                    headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_low"));
                }
                else if(signal >= 20){
                    headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_medium"));
                }
                else{
                    headsetSignalStatus.setTexture(texturesDAO.getTexture("headset_signal_high"));
                }

                if(battery > 0){
                    batteryStatus.setTexture(texturesDAO.getTexture("headset_battery_low"));
                }
                else{
                    batteryStatus.setTexture(texturesDAO.getTexture("headset_battery_full"));
                }
            }
        }
    }
}


void StatusVerifier::enable(){
    enabled = true;
}

void StatusVerifier::disable(){
    enabled = false;
}
