#include "GameDAO.hpp"

GameDAO::GameDAO(){
    loadRankings();
}


GameDAO::GameDAO(GameDAO const&){

}

GameDAO::~GameDAO(){
    //dtor
}

GameDAO& GameDAO::getInstance(){
    static GameDAO instance;
    return instance;
}


void GameDAO::saveGame(Game* game){

    std::string playerName = configurationDAO.getConfiguration("User");

    std::ofstream match {"data\\matches\\" + playerName
                                    + "_"
                                    + Utility::getYear()
                                    + Utility::getMonth()
                                    + Utility::getDay()
                                    + "_"
                                    + Utility::getTimestamp("")
                                    + ".txt"};

    match   << "Duration: "             << Utility::toString(game->getDuration()) << " seconds\n"
            << "Player: "               << playerName << "\n"
            << "Average Meditation: "   << game->getAverageMeditation() << "\n"
            << "Meditation values:\n\n";

    for(auto& med : game->getMeditationValues()){
        match << med << "\n";
    }

    match << "\nAttention values:\n\n";

    for(auto& att : game->getAttentionValues()){
        match << att << "\n";
    }

    if(configurationDAO.getConfiguration("ExportDatFiles") == "1"){
        std::ofstream meditationData {"data\\matches\\" + playerName
                                        + "_"
                                        + Utility::getYear()
                                        + Utility::getMonth()
                                        + Utility::getDay()
                                        + "_"
                                        + Utility::getTimestamp("")
                                        + "_meditation.dat"};
        std::ofstream attentionData {"data\\matches\\" + playerName
                                        + "_"
                                        + Utility::getYear()
                                        + Utility::getMonth()
                                        + Utility::getDay()
                                        + "_"
                                        + Utility::getTimestamp("")
                                        + "_attention.dat"};
        for(auto& med : game->getMeditationValues()){
            meditationData << med << "\n";
        }

        for(auto& att : game->getAttentionValues()){
            attentionData << att << "\n";
        }
    }

    MatchLog matchLog{playerName,
                       Utility::getDay() + "/" + Utility::getMonth() + "/" + Utility::getYear() + " " + Utility::getTimestamp(),
                       Utility::toString(game->getAverageMeditation()),
                       Utility::toString(game->getHeightPercentage()),
                       Utility::toString(game->getPerformance())};
    logMatch(matchLog, game->getDifficulty());
}

void GameDAO::loadRankings(){

    std::ifstream easy      {"data\\easy.dat"};
    std::ifstream medium    {"data\\medium.dat"};
    std::ifstream hard      {"data\\hard.dat"};

    std::string playerName = "", matchDate = "", avgMeditation = "", maxHeight = "", performance = "";

    while(getline(easy, playerName,      ';')){
        if(playerName != "\n"){
            getline(easy, matchDate,       ';');
            getline(easy, avgMeditation,   ';');
            getline(easy, maxHeight,       ';');
            getline(easy, performance,     ';');

            removeNewLines(&playerName);

            MatchLog newMatch{playerName, matchDate, avgMeditation, maxHeight, performance};
            easyRanking.push_back(newMatch);
        }
    }

    while(getline(medium, playerName,      ';')){
        if(playerName != "\n"){
            getline(medium, matchDate,       ';');
            getline(medium, avgMeditation,   ';');
            getline(medium, maxHeight,       ';');
            getline(medium, performance,     ';');

            removeNewLines(&playerName);

            MatchLog newMatch{playerName, matchDate, avgMeditation, maxHeight, performance};
            mediumRanking.push_back(newMatch);
        }
    }

    while(getline(hard, playerName,      ';')){
        if(playerName != "\n"){
            getline(hard, matchDate,       ';');
            getline(hard, avgMeditation,   ';');
            getline(hard, maxHeight,       ';');
            getline(hard, performance,     ';');

            removeNewLines(&playerName);

            MatchLog newMatch{playerName, matchDate, avgMeditation, maxHeight, performance};
            hardRanking.push_back(newMatch);
        }
    }

}

void GameDAO::logMatch(MatchLog match, int difficulty){

    std::vector<MatchLog> updatedRanking;

    if(difficulty == 0){
        updatedRanking = easyRanking;
    }
    else if(difficulty == 1){
        updatedRanking = mediumRanking;
    }
    else{
        updatedRanking = hardRanking;
    }

    sortMatch(&updatedRanking, match);
    std::ofstream actualRanking;

    if(difficulty == 0){
        easyRanking = updatedRanking;
        actualRanking.open("data\\easy.dat");
    }
    else if(difficulty == 1){
        mediumRanking = updatedRanking;
        actualRanking.open("data\\medium.dat");
    }
    else{

        hardRanking = updatedRanking;
        actualRanking.open("data\\hard.dat");
    }

    for(auto& matchLogged : updatedRanking){
        actualRanking << matchLogged.playerName     << ";"
                      << matchLogged.matchDate      << ";"
                      << matchLogged.avgMeditation  << ";"
                      << matchLogged.maxHeight      << ";"
                      << matchLogged.performance    << ";\n";
    }
    actualRanking << std::flush;

}

void GameDAO::sortMatch(std::vector<MatchLog>* ranking, MatchLog match){
    //std::vector<MatchLog>::iterator it = ranking->begin();

    int maxPos = (ranking->size() < 10) ? ranking->size() : 10;

    if(maxPos < 10){
        ranking->push_back(MatchLog{"---", "---", "0", "0", "0"});
        maxPos++;
    }

    for(int i = 0; i < maxPos; i++){
        //Empty slot, insert right away
        if(ranking->at(i).playerName == "" or ranking->at(i).playerName == "---"){
            ranking->at(i) = match;
            break;
        }
        //Greater performance comes first
        else if(Utility::StringToNumber(match.performance) > Utility::StringToNumber(ranking->at(i).performance)){
            MatchLog auxMatch = ranking->at(i);
            ranking->at(i) = match;
            sortMatch(ranking, auxMatch); //Re-sort the previous logged match into a new position (or excludes from the ranking)

            break;
        }
        //However, if the performance is the same...
        else if(Utility::StringToNumber(match.performance) == Utility::StringToNumber(ranking->at(i).performance)){
            //The one with greater average meditation comes first
            if(Utility::StringToNumber(match.avgMeditation) > Utility::StringToNumber(ranking->at(i).avgMeditation)){
                MatchLog auxMatch = ranking->at(i);
                ranking->at(i) = match;
                sortMatch(ranking, auxMatch);
                break;
            }
            //However, if both have the same average meditation...
            else if(Utility::StringToNumber(match.avgMeditation) == Utility::StringToNumber(ranking->at(i).avgMeditation)){
                //The one with greater height comes first
                if(Utility::StringToNumber(match.maxHeight) > Utility::StringToNumber(ranking->at(i).maxHeight)){
                    MatchLog auxMatch = ranking->at(i);
                    ranking->at(i) = match;
                    sortMatch(ranking, auxMatch);
                    break;
                }
                //However...
                else{
                    return; //Excluded from the ranking
                }
            }
        }
    }
}


std::vector<MatchLog> GameDAO::getRanking(int difficulty){
    if(difficulty == 0){
        return easyRanking;
    }
    else if(difficulty == 1){
        return mediumRanking;
    }
    else{
        return hardRanking;
    }
}


void GameDAO::removeNewLines(std::string* word){

    std::string::iterator it = word->begin();
    while(it != word->end()){
        if(*(it) == '\n'){
            it = word->erase(it);
        }
        else{
            it++;
        }
    }
}
