#include "ClockAnimation.hpp"

ClockAnimation::ClockAnimation(const float& x, const float& y, sf::Time _totalTime){

    for(int i = 1; i <= 21; i++){
        addFrame(sf::Sprite(texturesDAO.getTexture("clock_" + Utility::toString(i))));
    }

    totalTime = _totalTime.asMilliseconds();

    updateTime = sf::milliseconds(ceil(totalTime/(frames.size() - 1)));

    setPosition(sf::Vector2f(x, y));

    paused = true;

    std::cout << "Clock duration: " << getDuration().asMilliseconds() << std::endl;
    std::cout << "Clock update time: " << updateTime.asMilliseconds() << std::endl;
}

void ClockAnimation::startClock(){
    paused = false;
}

ClockAnimation::~ClockAnimation(){
    //dtor
}

