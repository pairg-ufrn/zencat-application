#include "player.hpp"

Player::Player(){
    force = 60;
    name = "Player";

    p_body.setSize(sf::Vector2f(PLAYER_SIZE, PLAYER_SIZE*2));

    p_body.setOrigin(p_body.getLocalBounds().width/2, p_body.getLocalBounds().height/2);

    p_sprite.setOrigin(p_body.getOrigin());

    p_sprite.setTexture(texturesDAO.getTexture("cat"));

    updateForce = 60;
}

Player::~Player(){
    //dtor
}

int Player::getForce(){
    return force;
}

float Player::getAverageForce(){
    float avg = 0.0;
    int n = 0;

    if(forceValues.size() > 0){
        for(auto& frc : forceValues){
            if(frc > 0){
                avg += frc;
                n++;
            }
        }
        avg /= n;
    }

    return avg;
}

std::vector<int> Player::getForceValues(){
    return forceValues;
}


void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(p_body);

}

void Player::setForce(int force){
    if(force <= MAX_FORCE and force >= MIN_FORCE){
        forceValues.push_back(force);
        this->updateForce = force;
    }
}

void Player::addForce(int _force){
    if(force <= MAX_FORCE and force >= MIN_FORCE){
        this->updateForce += _force;
    }
}

void Player::blink(){
    setForce(force + 10);
}

void Player::logBlink(int blinkStrength){
    forceValues.push_back(-1*blinkStrength);
}

std::string Player::getName(){
    return name;
}

void Player::setName(std::string name){
    this->name = name;
}



void Player::update(){ //do not update when forces are equal!!

    if(updateForceClock.getElapsedTime().asMilliseconds() > 150){

        updateForceClock.restart();

        if(updateForce > force){
            force++;
        }
        else if(updateForce < force){
            force--;
        }
    }
    return;
}

void Player::updateAnimations(float dt){

}

void Player::setPosition(sf::Vector2f newPos){

    p_position = newPos;
    p_body.setPosition(newPos);

    p_sprite.setPosition(newPos);

}

sf::Vector2f Player::getPosition(){
    return p_position;
}

void Player::applyForce(sf::Vector2f _force){
    setPosition(getPosition() + _force);
}
