#include "SoundPlayer.hpp"

SoundPlayer::SoundPlayer(){
    initializeSounds();

    setGlobalVolume(Utility::StringToNumber(configurationDAO.getConfiguration("Volume")));

}

SoundPlayer::~SoundPlayer(){
    //dtor
}

SoundPlayer& SoundPlayer::getInstance(){
    static SoundPlayer instance;
    return instance;
}

void SoundPlayer::play(const std::string& sound, const bool& loop){
    sounds.at(sound).play();
    sounds.at(sound).setLoop(loop);

}

void SoundPlayer::stop(const std::string& sound){
    sounds.at(sound).stop();
}

void SoundPlayer::stopAll(){
    for(auto& sound : sounds){
        sound.second.stop();
    }
}

void SoundPlayer::pauseAll(){
    for(auto& sound : sounds){
        sound.second.pause();
    }
}


void SoundPlayer::setVolume(const std::string& sound, const float& volume){
    sounds.at(sound).setVolume(volume);
    return;
}


void SoundPlayer::setGlobalVolume(const float& volume){

    sf::Listener::setGlobalVolume(volume);

    for(auto& sound : sounds){
        sound.second.setVolume(volume);
    }

    return;
}

void SoundPlayer::setPosition(const std::string& sound, const float& x, const float& y){
    sounds.at(sound).setPosition(x, y, 0);
}


void SoundPlayer::setMinimumDistance(const std::string& sound, const int& distance){
    sounds.at(sound).setMinDistance(distance);
    return;
}

void SoundPlayer::setAttenuation(const std::string& sound, const int& attenuation){
    sounds.at(sound).setAttenuation(attenuation);
    return;
}



void SoundPlayer::mute(){
    setGlobalVolume(0.f);
    return;
}

void SoundPlayer::initializeSounds(){
    std::vector<std::string> soundNames = soundsDAO.getBuffersNames();
    for(auto const& name : soundNames){
        sf::Sound sound;
        sound.setBuffer(soundsDAO.getSoundBuffer(name));
        sounds[name] = sound;
    }
    return;
}
