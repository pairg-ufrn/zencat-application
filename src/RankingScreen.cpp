#include "RankingScreen.hpp"

RankingScreen::RankingScreen(Application* app, const int& difficulty){
    this->app = app;

    this->app->statusVerifier.disable();

    background.setTexture(texturesDAO.getTexture("background_paws"));
    frame.setTexture(texturesDAO.getTexture("frame_ranking"));

    frame.setPosition(SCREEN_LENGHT/2 - frame.getLocalBounds().width/2, SCREEN_HEIGHT/2 - frame.getLocalBounds().height/2);

    returnButton.setTexture(texturesDAO.getTexture("button_back"));
    returnButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));
    returnButton.setPosition(EXIT_POSITION_X, EXIT_POSITION_Y);

    title.setCharacterSize(24U);
    title.setFont(fontsDAO.getFont("bromine"));
    title.setColor(sf::Color::Black);

    if(difficulty == 0){
        title.setString(translation(L"Easy"));
    }
    else if(difficulty == 1){
        title.setString(translation(L"Medium"));
    }
    else{
        title.setString(translation(L"Hard"));
    }

    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, SCREEN_HEIGHT*0.05);

    rankingPositionTitle.setFont(fontsDAO.getFont("bromine"));
    playerNameTitle.setFont     (fontsDAO.getFont("bromine"));
    matchDateTitle.setFont      (fontsDAO.getFont("bromine"));
    avgMeditationTitle.setFont  (fontsDAO.getFont("bromine"));
    maxHeightTitle.setFont      (fontsDAO.getFont("bromine"));

    rankingPositionTitle.setCharacterSize   (20U);
    playerNameTitle.setCharacterSize        (18U);
    matchDateTitle.setCharacterSize         (18U);
    avgMeditationTitle.setCharacterSize     (18U);
    maxHeightTitle.setCharacterSize         (18U);

    rankingPositionTitle.setColor   (sf::Color::Black);
    playerNameTitle.setColor        (sf::Color::Black);
    matchDateTitle.setColor         (sf::Color::Black);
    avgMeditationTitle.setColor     (sf::Color::Black);
    maxHeightTitle.setColor         (sf::Color::Black);

    rankingPositionTitle.setString  ("#");
    playerNameTitle.setString       (translation(L"Name"));
    matchDateTitle.setString        (translation(L"Date"));
    avgMeditationTitle.setString    (translation(L"Meditation"));
    maxHeightTitle.setString        (translation(L"Height"));

    //headerText.setString("Name\tDate\tAvg.Meditation\tMax.Height\tPerformance");
    //headerText.setPosition(SCREEN_LENGHT/2 - headerText.getLocalBounds().width/2, frame.getPosition().y + frame.getLocalBounds().height*0.15);

    std::vector<MatchLog> matches = gameDAO.getRanking(difficulty);

    unsigned int fontSize = 17U;
    int position = 1;
    for(auto& match : matches){
        if(match.playerName != "" and match.playerName != "\n"){
            RankingRow newRow {sf::Text(Utility::toString(position++),  fontsDAO.getFont("bromine"), fontSize),
                               sf::Text(match.playerName,               fontsDAO.getFont("bromine"), fontSize),
                               sf::Text(match.matchDate,                fontsDAO.getFont("bromine"), fontSize - 2),
                               sf::Text(match.avgMeditation,            fontsDAO.getFont("bromine"), fontSize),
                               sf::Text(match.maxHeight,                fontsDAO.getFont("bromine"), fontSize),
                               sf::Text(match.performance,              fontsDAO.getFont("bromine"), fontSize)};
            adjustPlayerNameSize(&newRow.playerName);
            rows.push_back(newRow);
        }
    }

    tableTop.setTexture(texturesDAO.getTexture("ranking_row_top"));

    formatRows();

}

RankingScreen::~RankingScreen(){
    app->statusVerifier.enable();
}

void RankingScreen::draw(const float dt){

    app->window.draw(background);
    app->window.draw(frame);
    app->window.draw(title);
    app->window.draw(returnButton);

    app->window.draw(tableTop);
    app->window.draw(rankingPositionTitle);
    app->window.draw(playerNameTitle);
    app->window.draw(matchDateTitle);
    app->window.draw(avgMeditationTitle);
    app->window.draw(maxHeightTitle);

    for(auto& row : rows){
        app->window.draw(row.frame);
        app->window.draw(row.position);
        app->window.draw(row.playerName);
        app->window.draw(row.matchDate);
        app->window.draw(row.avgMeditation);
        app->window.draw(row.maxHeight);
    }
}

void RankingScreen::update(const float dt){

}

void RankingScreen::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(returnButton.clicked(mouse.x, mouse.y)){
                app->popState();
            }
        } break;

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            returnButton.hover(mouse.x, mouse.y);

            break;
        }
    }

}

void RankingScreen::formatRows(){
    int n = rows.size();
    int level = 0;
    int i, posX, posY;

    int margin = frame.getLocalBounds().width*0.06;

    tableTop.setPosition(frame.getPosition().x + frame.getLocalBounds().width/2 - tableTop.getTexture()->getSize().x/2,
                         frame.getPosition().y + frame.getLocalBounds().height*0.08);

    float centerY = tableTop.getPosition().y + static_cast<int>(tableTop.getLocalBounds().height/2) - 4;

    rankingPositionTitle.setPosition(tableTop.getPosition().x + margin/2,
                                     centerY - rankingPositionTitle.getLocalBounds().height/2);

    playerNameTitle.setPosition(tableTop.getPosition().x + static_cast<int>(tableTop.getLocalBounds().width*0.15),
                                centerY - static_cast<int>(playerNameTitle.getLocalBounds().height/2));

    matchDateTitle.setPosition(tableTop.getPosition().x + static_cast<int>(tableTop.getLocalBounds().width*0.43),
                               centerY - static_cast<int>(matchDateTitle.getLocalBounds().height/2));

    avgMeditationTitle.setPosition(tableTop.getPosition().x + static_cast<int>(tableTop.getLocalBounds().width*0.66),
                                   centerY - static_cast<int>(avgMeditationTitle.getLocalBounds().height/2));

    maxHeightTitle.setPosition(tableTop.getPosition().x + static_cast<int>(tableTop.getLocalBounds().width*0.87),
                               centerY - static_cast<int>(avgMeditationTitle.getLocalBounds().height/2));

    for(i = 0; i < n; i++){
        if(i%2 == 0){
            rows.at(i).frame.setTexture(texturesDAO.getTexture("ranking_row_1"));
        }
        else{
            rows.at(i).frame.setTexture(texturesDAO.getTexture("ranking_row_2"));
        }

        posX = frame.getPosition().x + frame.getLocalBounds().width/2 - rows.at(i).frame.getTexture()->getSize().x/2;
        posY = frame.getPosition().y + frame.getLocalBounds().height*0.182 + frame.getLocalBounds().height*0.075*level;

        rows.at(i).frame.setPosition(posX, posY);

        rows.at(i).position.setPosition       ( rankingPositionTitle.getPosition().x
                                              + static_cast<int>(rankingPositionTitle.getLocalBounds().width/2)
                                              - static_cast<int>(rows.at(i).position.getLocalBounds().width/2)
                                                ,
                                                rows.at(i).frame.getPosition().y
                                              + static_cast<int>(rows.at(i).frame.getLocalBounds().height/2)
                                              - static_cast<int>(rows.at(i).position.getLocalBounds().height/2) - 4);

        rows.at(i).playerName.setPosition     ( playerNameTitle.getPosition().x
                                              + static_cast<int>(playerNameTitle.getLocalBounds().width/2)
                                              - static_cast<int>(rows.at(i).playerName.getLocalBounds().width/2)
                                                ,
                                                rows.at(i).frame.getPosition().y
                                              + static_cast<int>(rows.at(i).frame.getLocalBounds().height/2)
                                              - static_cast<int>(rows.at(i).playerName.getLocalBounds().height/2) - 3);

        rows.at(i).matchDate.setPosition      ( matchDateTitle.getPosition().x
                                              + static_cast<int>(matchDateTitle.getLocalBounds().width/2)
                                              - static_cast<int>(rows.at(i).matchDate.getLocalBounds().width/2)
                                                ,
                                                rows.at(i).frame.getPosition().y
                                              + static_cast<int>(rows.at(i).frame.getLocalBounds().height/2)
                                              - static_cast<int>(rows.at(i).matchDate.getLocalBounds().height/2) - 2);

        rows.at(i).avgMeditation.setPosition  ( avgMeditationTitle.getPosition().x
                                              + static_cast<int>(avgMeditationTitle.getLocalBounds().width/2)
                                              - static_cast<int>(rows.at(i).avgMeditation.getLocalBounds().width/2)
                                                ,
                                                rows.at(i).frame.getPosition().y
                                              + static_cast<int>(rows.at(i).frame.getLocalBounds().height/2)
                                              - static_cast<int>(rows.at(i).avgMeditation.getLocalBounds().height/2) - 4);

        rows.at(i).maxHeight.setPosition      ( maxHeightTitle.getPosition().x
                                              + static_cast<int>(maxHeightTitle.getLocalBounds().width/2)
                                              - static_cast<int>(rows.at(i).maxHeight.getLocalBounds().width/2)
                                                ,
                                                rows.at(i).frame.getPosition().y
                                              + static_cast<int>(rows.at(i).frame.getLocalBounds().height/2)
                                              - static_cast<int>(rows.at(i).maxHeight.getLocalBounds().height/2) - 4);

        rows.at(i).position.setColor(sf::Color::Black);
        rows.at(i).playerName.setColor(sf::Color::Black);
        rows.at(i).matchDate.setColor(sf::Color::Black);
        rows.at(i).avgMeditation.setColor(sf::Color::Black);
        rows.at(i).maxHeight.setColor(sf::Color::Black);

        level++;
    }
}



void RankingScreen::adjustPlayerNameSize(sf::Text* playerName){
    unsigned int characterSize = playerName->getCharacterSize();

    while(playerName->getLocalBounds().width > SCREEN_LENGHT*0.15){
        characterSize--;
        playerName->setCharacterSize(characterSize);
    }
}

