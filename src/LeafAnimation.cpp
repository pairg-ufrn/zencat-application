#include "LeafAnimation.hpp"

LeafAnimation::LeafAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("leaf_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("leaf_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("leaf_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("leaf_4")));
    addFrame(sf::Sprite(texturesDAO.getTexture("leaf_5")));
    addFrame(sf::Sprite(texturesDAO.getTexture("leaf_6")));

    setPosition(sf::Vector2f(x, y));

    name = "leaf";

    paused = false;

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        flipX();
    }

    //soundPlayer.play("leaf");

}

LeafAnimation::~LeafAnimation(){
    //dtor
}

void LeafAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction*3, 0));
}

