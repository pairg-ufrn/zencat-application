#include "AirshipAnimation.hpp"

AirshipAnimation::AirshipAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("airship")));

    name = "airship";

    setPosition(sf::Vector2f(x, y));

    paused = false;

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        flipX();
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0));
    }

    //soundPlayer.play("airship");
}

AirshipAnimation::~AirshipAnimation(){
    //dtor
}


void AirshipAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction, 0));
}
