#include "Application.hpp"

void Application::pushState(State* state){
    this->states.push(state);
    return;
}

void Application::popState(){

    delete this->states.top();
    this->states.pop();

    return;
}

void Application::changeState(State* state){
    if(!this->states.empty())
        popState();
    pushState(state);

    return;
}

State* Application::peekState() {
    if(this->states.empty()) return nullptr;
    return this->states.top();
}

void Application::mainLoop() {

    sf::Event event;

    sf::Clock clock;

    sf::Time elapsed;

    float dt;

    while(this->window.isOpen()){

        elapsed = clock.restart();
        dt = elapsed.asSeconds();

        if(peekState() == nullptr) continue;

        while(window.pollEvent(event)){

            handleEvent(event);
            peekState()->handleEvent(event);

        }

        peekState()->update(dt);

        //videoHandler.update();

        statusVerifier.checkHeadsetStatus();

        this->window.clear(sf::Color::Black);

        peekState()->draw(dt);

        window.setView(defaultView);
        draw();

        this->window.display();
    }
}

void Application::draw(){

    if(globalWarningClock.getElapsedTime().asSeconds() < 4){
        window.draw(globalWarning);
    }

    window.draw(statusVerifier);
    //videoHandler.draw(&window);

}


void Application::handleEvent(sf::Event& event){

    if(event.type == sf::Event::KeyPressed){
        switch(event.key.code){

            case sf::Keyboard::F12:
                takeScreenshot();
            break;
        }
    }
    else if(event.type == sf::Event::Resized){
        cout << "Width: " << event.size.width << "\nHeight: " << event.size.height << endl << endl;
    }

}

void Application::setGlobalMessage(const std::string& message){
    globalWarning.setString(message);
    globalWarning.setPosition(sf::Vector2f(static_cast<int>(SCREEN_LENGHT*0.5 - globalWarning.getGlobalBounds().width*0.5),
                                           static_cast<int>(SCREEN_HEIGHT*0.9)));
    globalWarningClock.restart();
}


void Application::takeScreenshot(){
    sf::Image screenshot = window.capture();

    std::string screenshotName = "ZC_ScreenShot_" + Utility::getDatestamp("-") + "_" + Utility::getTimestamp("");

    screenshot.saveToFile("screenshots/" + screenshotName + ".png");

    setGlobalMessage(translation(L"Screenshot saved!"));

    cout << "ScreenShot saved!" << endl;
}


Application::Application() {

    globalWarning.setFont(fontsDAO.getFont("agency_bold"));
    globalWarning.setColor(sf::Color::Red);
    globalWarning.setCharacterSize(24U);
    globalWarning.setPosition(SCREEN_LENGHT*0.4, SCREEN_HEIGHT*0.9);
    globalWarning.setString("");

    sf::Image icon;

    icon.loadFromFile("media/textures/icon.png");

    mindwaveModule.connect(configurationDAO.getConfiguration("COMPort").c_str());

    window.create(sf::VideoMode(SCREEN_LENGHT, SCREEN_HEIGHT), GAME_TITLE, sf::Style::Close);
    window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
    window.setFramerateLimit(FRAMERATE);

    applicationView.setSize(SCREEN_LENGHT, SCREEN_HEIGHT);
    defaultView = window.getDefaultView();


}

Application::~Application(){
    while(!this->states.empty()) popState();
    mindwaveModule.disconnect();
}
