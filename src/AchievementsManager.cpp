#include "AchievementsManager.hpp"

AchievementsManager& AchievementsManager::getInstance(){
    static AchievementsManager instance;
    return instance;
}

AchievementsManager::AchievementsManager(){
    loadAchievements();

}

AchievementsManager::~AchievementsManager(){
    saveAchievements();
}

bool AchievementsManager::unlockedAchievement(){
    return unlockedNewAchievement;
}

void AchievementsManager::setUnlocked(bool unlocked){
    unlockedNewAchievement = unlocked;
}


std::vector<Achievement> AchievementsManager::getAchievements(){
    return achievements;
}


void AchievementsManager::trackAchievements(Game* game){
    for(auto& achiev : achievements){

        if(not achiev.obtained){

            if(achiev.condition(game)){
                achiev.obtained = true;
                achiev.recentlyObtained = true;
                unlockedNewAchievement = true;
            }

        }

    }

}


void AchievementsManager::saveAchievements(){
    std::string save;
    std::ofstream output{"data\\achievements.dat"};
    for(auto& achiev : achievements){
        if(achiev.obtained){
            save += "1";
        }
        else{
            save += "0";
        }
    }

    //save = Utility::encryptDecrypt(save);

    output << save;
}


//The difficulty is "easy" and the player won the match
bool winOnEasyCondition(Game* game){
    if(game->getDifficulty() == 0 and game->getResult()){
        return true;
    }
    return false;
}


//The difficulty is "medium" and the player won the match
bool winOnMediumCondition(Game* game){
    if(game->getDifficulty() == 1 and game->getResult()){
        return true;
    }
    return false;
}


//The difficulty is "hard" and the player won the match
bool winOnHardCondition(Game* game){
    if(game->getDifficulty() == 2 and game->getResult()){
        return true;
    }
    return false;
}


//Search for ten or more values of 100 in the meditation vector.
bool hundredMeditationCondition(Game* game){
    int hundredCount = 0;

    for(auto& value : game->getMeditationValues()){
        if(value == 100){
            hundredCount++;
        }
    }

    if(hundredCount >= 10){
        return true;
    }
    return false;
}

bool keepLowCondition(Game* game){
    if(game->getMeditationValues().size() >= 10 and game->getAverageMeditation(10) <= 15){
        return true;
    }
    return false;
}


//If the average of the last 10 values is above 85.
bool keepHighCondition(Game* game){
    if(game->getMeditationValues().size() >= 10 and game->getAverageMeditation(10) >= 85){
        return true;
    }
    return false;
}


//Reached the Beyond on easy.
bool transcendent1Condition(Game* game){
    if(game->getDifficulty() == 0 and game->getPlayerHeight() >= game->getMaximumHeight()*(4.f/5)){
        return true;
    }
    return false;
}


//Reached the Beyond on medium.
bool transcendent2Condition(Game* game){
    if(game->getDifficulty() == 1 and game->getPlayerHeight() >= game->getMaximumHeight()*(4.f/5)){
        return true;
    }
    return false;
}


//Reached the Beyond on hard.
bool transcendent3Condition(Game* game){
    if(game->getDifficulty() == 2 and game->getPlayerHeight() >= game->getMaximumHeight()*(4.f/5)){
        return true;
    }
    return false;
}

//To avoid having to create an "Achievement" class with virtual functions, I chose to use pointers to functions.
void AchievementsManager::loadAchievements(){
    Achievement winOnEasy           {   false,
                                        false,
                                        translation(L"Zen Town"),
                                        translation(L"Win a game on the Easy difficulty."),
                                        "achievement_win_easy",
                                        winOnEasyCondition};
    achievements.push_back(winOnEasy);


    Achievement winOnMedium         {   false,
                                        false,
                                        translation(L"Zen Mountain"),
                                        translation(L"Win a game on the Medium difficulty."),
                                        "achievement_win_medium",
                                        winOnMediumCondition};
    achievements.push_back(winOnMedium);


    Achievement winOnHard           {   false,
                                        false,
                                        translation(L"Zen Temple"),
                                        translation(L"Win a game on the Hard difficulty."),
                                        "achievement_win_hard",
                                        winOnHardCondition};
    achievements.push_back(winOnHard);


    Achievement keepLow             {   false,
                                        false,
                                        translation(L"Zen Disaster"),
                                        translation(L"Keep your average meditation below 15 for 10 seconds."),
                                        "achievement_low",
                                        keepLowCondition};
    achievements.push_back(keepLow);


    Achievement hundredMeditation   {   false,
                                        false,
                                        translation(L"ThouZENd"),
                                        translation(L"Reach 100 meditation ten times."),
                                        "achievement_hundred",
                                        hundredMeditationCondition};
    achievements.push_back(hundredMeditation);


    Achievement keepHigh            {   false,
                                        false,
                                        translation(L"Zen Master"),
                                        translation(L"Keep your average meditation above 85 for 10 seconds."),
                                        "achievement_high",
                                        keepHighCondition};
    achievements.push_back(keepHigh);


    Achievement transcendent1        {  false,
                                        false,
                                        translation(L"Revelation"),
                                        translation(L"Reach the Beyond on the Easy difficulty."),
                                        "achievement_transcendent_easy",
                                        transcendent1Condition};
    achievements.push_back(transcendent1);


    Achievement transcendent2        {  false,
                                        false,
                                        translation(L"Ascension"),
                                        translation(L"Reach the Beyond on the Medium difficulty."),
                                        "achievement_transcendent_medium",
                                        transcendent2Condition};
    achievements.push_back(transcendent2);


    Achievement transcendent3        {  false,
                                        false,
                                        translation(L"Nirvana"),
                                        translation(L"Reach the Beyond on the Hard difficulty."),
                                        "achievement_transcendent_hard",
                                        transcendent3Condition};
    achievements.push_back(transcendent3);


    std::ifstream input {"data\\achievements.dat"};

    std::string achievs;

    input >> achievs;

    //achievs = Utility::encryptDecrypt(achievs);

    int n = achievs.length();
    for(int i = 0; i < n; i++){
        if(achievs.at(i) == '1'){
            achievements.at(i).obtained = true;
        }
    }
}


//To load the translated strings again
void AchievementsManager::reloadAchievements(){
    achievements.clear();
    loadAchievements();
}
