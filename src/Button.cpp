#include "button.hpp"

Button::Button(){
    b_size = sf::Vector2f(0, 0);
    b_text.setFont(fontsDAO.getFont("agency_bold"));
    b_text.setCharacterSize(20U);
    b_text.setColor(sf::Color::White);
    b_text.setString("");

    updateTextPosition();
}

Button::~Button(){

}

void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const {
    if(not isInvisible){
        //target.draw(b_body);
        target.draw(b_sprite);
        target.draw(b_text);
    }
    return;
}

Button::Button(int x, int y, int size_x, int size_y, std::string desc, bool centerHorizontally, bool centerVertically){

    setPosition(x, y);
    setSize(size_x, size_y);

    b_text.setFont(fontsDAO.getFont("bubblegum"));

    setText(desc);
    b_text.setCharacterSize(20U);
    b_text.setColor(sf::Color::White);
    std::string filename = "";

    updateTextPosition();
}

void Button::enable(){
    enabled = true;
    return;
}

void Button::disable(){
    enabled = false;
    setColor(sf::Color(200, 200, 200, 150));
    return;
}

void Button::setInvisible(const bool& inv){
    isInvisible = inv;
    return;
}

bool Button::contains(const float& x, const float& y){
    sf::FloatRect body(getPosition().x, getPosition().y, b_size.x, b_size.y);
    if(body.contains(x, y)){
        return true;
    }
    else{
        return false;
    }
}

bool Button::clicked(const float& x, const float& y){
    if(contains(x, y) and sf::Mouse::isButtonPressed(sf::Mouse::Left)){
        if(enabled){
            soundPlayer.play("bip");
            return true;
        }
    }

    return false;
}


/**
Changes the texture and plays a sound when the mouse hover over the button.
The button is enabled for hover by default, and must be explicitly disabled using the disableHover function if necessary.
Returns true if the coordinates are within the button area, the button is enabled and is set to change on hover; false otherwise.
**/
bool Button::hover(const float& x, const float& y){
    if(enabled and changeOnHover){
        if(contains(x, y)){
            if(not hovered){ //The "hovered" variable ensures that the processing is done only once when entering or leaving the button area.
                setColor(sf::Color::Green);
                b_sprite.setTexture(b_hoverTexture, true);
                soundPlayer.play("select");
                hovered = true;
                return true;
            }
        }
        else{
            if(hovered){
                setColor(sf::Color::White);
                b_sprite.setTexture(b_texture, true);
                hovered = false;
                setPosition(b_position);
            }

        }
    }
    return false;
}

void Button::disableHover(){
    changeOnHover = false;
    return;
}

void Button::setFontSize(const unsigned int& _size){
    b_text.setCharacterSize(_size);
    updateTextPosition();
}

/**
Sets the click-able area of the button to the size of the current text.
**/
void Button::setSizeToText(){
    sf::Vector2f newSize(b_text.getLocalBounds().width, b_text.getLocalBounds().height);
    b_size = newSize;
    b_body.setSize(b_size);
}

void Button::setSize(const float& x, const float& y){
    b_size.x = x;
    b_size.y = y;
    b_body.setSize(b_size);
    //b_body.setOrigin(static_cast<int>(b_body.getLocalBounds().width/2), static_cast<int>(b_body.getLocalBounds().height/2));
    //b_sprite.setOrigin(b_body.getOrigin());
    updateTextPosition();
}

void Button::setPosition(const float& x, const float& y){
    setPosition(sf::Vector2f(x, y));
}


void Button::setPosition(const sf::Vector2f& position){
    b_position.x = position.x;
    b_position.y = position.y;
    b_body.setPosition(b_position);
    b_sprite.setPosition(b_position);
    b_text.setPosition(b_position);
    updateTextPosition();
}

void Button::setText(const std::string& text){
    b_string = text;
    b_text.setString(b_string);
    updateTextPosition();
}

void Button::setFont(const sf::Font& font){
    b_font = font;
    b_text.setFont(b_font);
    updateTextPosition();
}

void Button::setTextColor(sf::Color color){
    b_text.setColor(color);
}

void Button::setTextColor(const UINT8& red, const UINT8& green, const UINT8& blue, const UINT8& alpha){
    b_text.setColor(sf::Color(red, green, blue, alpha));
}


void Button::setColor(const sf::Color& color){
    b_color = color;
    b_body.setFillColor(color);
}

void Button::setTexture(const sf::Texture& texture){
    b_texture = texture;
    b_sprite.setTexture(texture);

    if(texture.getSize().x > 0 and texture.getSize().y > 0){
        setSize(texture.getSize().x, texture.getSize().y);
    }
}

void Button::setHoverTexture(const sf::Texture& texture){
    b_hoverTexture = texture;
    changeOnHover = true;
}

sf::Vector2f Button::getPosition(){
    return b_position;
}

sf::Vector2f Button::getSize(){
    return b_size;
}

/**
Update the text's position after changing its properties, such as size, font and string.
The origin is set to the center of the new text, and the new position is in the center of the button.
**/
void Button::updateTextPosition() {
    if (b_size.x > 0 && b_size.y > 0 && b_text.getString() != "") {
        b_text.setOrigin(b_text.getLocalBounds().left + b_text.getLocalBounds().width/2,
                         b_text.getLocalBounds().top + b_text.getLocalBounds().height/2);
        b_text.setPosition(b_position.x + b_body.getLocalBounds().width/2,
                           b_position.y + b_body.getLocalBounds().height/2);
    }
}

void Button::setVerticalPosition(const unsigned int& yPos){
    setPosition(getPosition().x, yPos);
}

void Button::setHorizontalPosition(const unsigned int& xPos){
    setPosition(xPos, getPosition().y);
}

/**
Do not use any of the bellow functions, as the origin is not set to the center of the button.
If you choose to set the origin of the button to the center, you must also subtract getSize()*0.5 from "body" variable of the the "contains" function.
You can also just adjust the functions bellow to work in the current mode, it's plain simple actually.
**/
void Button::alignToLeft(){
    setPosition(static_cast<int>(getSize().x*0.5), getPosition().y);
}

void Button::alignToRight(){
    setPosition(SCREEN_LENGHT - static_cast<int>(getSize().x*0.5), getPosition().y);
}

void Button::alignToTop(){
    setPosition(getPosition().x, static_cast<int>(getSize().y*0.5));
}

void Button::alignToBottom(){
    setPosition(getPosition().x, SCREEN_HEIGHT - static_cast<int>(getSize().y*0.5));
}

void Button::alignToCenter(){
    setPosition(SCREEN_LENGHT/2, SCREEN_HEIGHT/2);
}

