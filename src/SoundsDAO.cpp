#include "soundsDAO.hpp"


SoundsDAO& SoundsDAO::getInstance(){
    static SoundsDAO instance;
    return instance;
}

SoundsDAO::~SoundsDAO(){
    buffers.clear();
}

void SoundsDAO::loadSoundBuffers(){

    loadSoundBuffer("airplane", "airplane.wav");
    loadSoundBuffer("blink",    "blink.wav");
    loadSoundBuffer("bip",      "bip.wav");
    loadSoundBuffer("bird1",    "bird1.wav");
    loadSoundBuffer("comet",    "comet.wav");
    loadSoundBuffer("count",    "count.wav");
    loadSoundBuffer("deselect", "deselect.wav");
    loadSoundBuffer("dragonfly","dragonfly.wav");
    loadSoundBuffer("logo",     "logo_1.wav");
    loadSoundBuffer("lose",     "lose.wav");
    loadSoundBuffer("ovni",     "ovni.wav");
    loadSoundBuffer("select",   "select.wav");
    loadSoundBuffer("spacecraft", "spacecraft.wav");
    loadSoundBuffer("splash_1", "splash_1.wav");
    loadSoundBuffer("waterfall","waterfall.wav");
    loadSoundBuffer("win",      "win.wav");

    return;
}

void SoundsDAO::loadSoundBuffer(const std::string& name, const std::string& filename){

    sf::SoundBuffer buffer;
    buffer.loadFromFile("media/sounds/" + filename);

    this->buffers[name] = buffer;

    return;
}

sf::SoundBuffer& SoundsDAO::getSoundBuffer(const std::string& buffer){
    try{
        return this->buffers.at(buffer);
    }
    catch(std::out_of_range& e){
        return buffers.at("bip");
    }
}

std::vector<std::string> SoundsDAO::getBuffersNames(){
    std::vector<std::string> names;
    for(auto const& name : buffers){
        names.push_back(name.first);
    }
    return names;
}

