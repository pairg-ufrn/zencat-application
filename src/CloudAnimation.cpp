#include "CloudAnimation.hpp"

CloudAnimation::CloudAnimation(const float& x, const float& y, const int& scenarioID, const int& cloudID){
    addFrame(sf::Sprite(texturesDAO.getTexture("cloud_BG"+ Utility::toString(scenarioID) + "_" + Utility::toString(cloudID))));

    setPosition(sf::Vector2f(x, y));

    name = "cloud";

    paused = false;

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
    }
    velocity = 0.10f + (rand()%90)/90.f;
}

CloudAnimation::~CloudAnimation(){
    //dtor
}

void CloudAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction*velocity, 0));
}
