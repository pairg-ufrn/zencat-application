#include "SplashScreen.hpp"

SplashScreen::SplashScreen(Application* app){
    this->app = app;

    splash.setTexture(texturesDAO.getTexture("splash"));

    splashClock.restart();

    app->statusVerifier.disable();

}

SplashScreen::~SplashScreen(){
    app->statusVerifier.disable();
}

void SplashScreen::draw(const float dt){
    app->window.draw(splash);
}

void SplashScreen::update(const float dt){
    if(splashClock.getElapsedTime().asMilliseconds() < 4500){
        splash.setColor(sf::Color(color, color, color, alpha));

        if(splashClock.getElapsedTime().asMilliseconds() < 3000){
            if(color < 255){
                color += 3;
                alpha += 3;
            }
        }
        else{
            if(color > 0){
                color -= 3;
                alpha -= 3;
            }

        }

        if( playSong and splashClock.getElapsedTime().asMilliseconds() > 500){
            soundPlayer.play("logo");
            playSong = false;
        }
    }
    else{
        app->popState();
    }
}


void SplashScreen::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;
    }
}
