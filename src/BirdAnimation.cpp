#include "BirdAnimation.hpp"

BirdAnimation::BirdAnimation(const float& x, const float& y) : HorizontalAnimation(x, y){
    std::string color = "";

    srand(time(NULL));
    int randomColor = rand()%1000;

    if(randomColor > 500){
        color = "_yellow";
    }
    addFrame(sf::Sprite(texturesDAO.getTexture("bird_1" + color)));
    addFrame(sf::Sprite(texturesDAO.getTexture("bird_2" + color)));
    addFrame(sf::Sprite(texturesDAO.getTexture("bird_3" + color)));

    setPosition(sf::Vector2f(x, y));

    name = "bird";

    paused = false;

    updateTime = sf::milliseconds(150);

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        flipX();
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0));
    }

    soundPlayer.play("bird1");

}

BirdAnimation::~BirdAnimation(){
    soundPlayer.stop("bird1");
}

