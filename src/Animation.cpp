#include "Animation.hpp"


Animation::Animation(const float& x, const float& y, sf::Time _updateTime){
    currentFrame = 0;
    paused = true;
    updateTime = _updateTime;
    setPosition(sf::Vector2f(x, y));
    name = "";
}

Animation::~Animation(){
    //dtor
}

std::string Animation::getName(){
    return name;
}

void Animation::addFrame(sf::Sprite frame){
    frames.push_back(frame);
}

void Animation::play(){
    paused = false;
}

void Animation::pause(){
    paused = true;
}

void Animation::stop(){
    paused = true;
    currentFrame = 0;
}

void Animation::clearFrames(){
    frames.clear();
}

void Animation::setPosition(sf::Vector2f pos){

    position = pos;

    for(auto& frame : frames){
        frame.setPosition(pos);
    }
}

sf::Vector2f Animation::getPosition(){
    return position;
}

sf::Time Animation::getDuration(){
    return (updateTime*static_cast<float>(frames.size()));
}

void Animation::flipX(){
    for(auto& frame : frames){
        int width = frame.getTexture()->getSize().x;
        int height = frame.getTexture()->getSize().y;
        frame.setTextureRect(sf::IntRect(width, 0, -width, height));
    }
}

void Animation::unflipX(){
    for(auto& frame : frames){
        int width = frame.getTexture()->getSize().x;
        int height = frame.getTexture()->getSize().y;
        frame.setTextureRect(sf::IntRect(0, 0, width, height));
    }
}

void Animation::update(const float& dt){
    currentTime += sf::seconds(dt);

    if(currentTime >= updateTime){
        currentTime = sf::Time::Zero;

        currentFrame++;
        currentFrame %= frames.size();
    }
    loop(dt);
}

void Animation::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    target.draw(frames.at(currentFrame));
}
