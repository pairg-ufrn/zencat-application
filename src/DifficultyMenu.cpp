#include "difficultyMenu.hpp"

DifficultyMenu::DifficultyMenu(Application* app,
                               bool _unlockedAnimation): nextButton     (SCREEN_LENGHT*0.9,  SCREEN_HEIGHT*0.45, 48, 48, ""),
                                                         previousButton (SCREEN_LENGHT*0.05, SCREEN_HEIGHT*0.45, 48, 48, ""),
                                                         backButton     (EXIT_POSITION_X, EXIT_POSITION_Y, 48, 48, ""),
                                                         playButton     (SCREEN_LENGHT*0.4,  SCREEN_HEIGHT*0.82, 85, 85, "GO!"){
    this->app = app;

    app->statusVerifier.enable();

    unlockedDifficulty = static_cast<int>(Utility::StringToNumber(configurationDAO.getConfiguration("Unlocked")));
    unlockedAnimation = _unlockedAnimation;

    background.setTexture(texturesDAO.getTexture("background_difficulty"));

    frame.setTexture(texturesDAO.getTexture("frame_cat"));
    frame.setPosition(SCREEN_LENGHT/2 - frame.getTexture()->getSize().x/2,
                      SCREEN_HEIGHT/2 - frame.getTexture()->getSize().y*0.575);

    padlock.setTexture(texturesDAO.getTexture("padlock"));
    padlock.setOrigin(static_cast<int>(padlock.getGlobalBounds().width/2),
                      static_cast<int>(padlock.getGlobalBounds().height/2));
    padlock.setPosition(sf::Vector2f(static_cast<int>(SCREEN_LENGHT*0.5),
                                     static_cast<int>(SCREEN_HEIGHT*0.5)));

    title.setCharacterSize(30U);
    title.setColor(sf::Color::White);
    title.setFont(fontsDAO.getFont("bromine"));
    title.setString(translation(L"Easy"));
    title.setPosition(SCREEN_LENGHT/2 - title.getLocalBounds().width/2, frame.getPosition().y + frame.getLocalBounds().height*0.05);

    difficultyBG.setTexture(texturesDAO.getTexture("easy"));
    difficultyBG.setPosition(SCREEN_LENGHT/2 - difficultyBG.getTexture()->getSize().x/2,
                             SCREEN_HEIGHT/2 - difficultyBG.getTexture()->getSize().y/2 - 16);


    nextButton.setColor(sf::Color::Green);
    nextButton.setFontSize(20U);

    previousButton.setColor(sf::Color::Yellow);
    previousButton.setFontSize(20U);

    playButton.setTexture(texturesDAO.getTexture("button_go"));
    backButton.setTexture(texturesDAO.getTexture("button_back"));

    playButton.setHoverTexture(texturesDAO.getTexture("button_go_hover"));
    backButton.setHoverTexture(texturesDAO.getTexture("button_back_hover"));

    nextButton.setTexture(texturesDAO.getTexture("button_right"));
    previousButton.setTexture(texturesDAO.getTexture("button_left"));

    nextButton.setHoverTexture(texturesDAO.getTexture("button_right_hover"));
    previousButton.setHoverTexture(texturesDAO.getTexture("button_left_hover"));

    playButton.setPosition(static_cast<int>(SCREEN_LENGHT/2 - playButton.getSize().x/2), SCREEN_HEIGHT*0.84);
    playButton.setFontSize(36U);

    rankingButton.setTexture(texturesDAO.getTexture("button_ranking"));
    rankingButton.setHoverTexture(texturesDAO.getTexture("button_ranking_hover"));
    rankingButton.setPosition(SCREEN_LENGHT*0.98 - rankingButton.getSize().x, SCREEN_HEIGHT*0.83);

    unlockedAnimationClock.restart();
    if((selectedDifficulty > unlockedDifficulty) or unlockedAnimation){
        showPadlock = true;
    }

    //playButton.disable();

}

DifficultyMenu::~DifficultyMenu(){
    app->statusVerifier.disable();
}

void DifficultyMenu::draw(const float dt){
    app->window.draw(background);

    app->window.draw(difficultyBG);
    app->window.draw(frame);
    app->window.draw(title);
    app->window.draw(nextButton);
    app->window.draw(previousButton);
    app->window.draw(backButton);
    app->window.draw(playButton);
    app->window.draw(rankingButton);

    if(showPadlock){
        app->window.draw(padlock);

    }

    return;
}

void DifficultyMenu::update(const float dt){
    if((selectedDifficulty > unlockedDifficulty) or unlockedAnimation){
        showPadlock = true;
    }
    else{
        showPadlock = false;
    }

    if(unlockedAnimation){
        runUnlockedAnimation(unlockedDifficulty);
    }
}

void DifficultyMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(nextButton.clicked(mouse.x, mouse.y)){
                ++selectedDifficulty %= 3;
                setDifficulty(selectedDifficulty);
            }

            if(previousButton.clicked(mouse.x, mouse.y)){
                if(selectedDifficulty-- == 0){
                    selectedDifficulty = static_cast<int>(DIFFICULTY::HARD);
                }
                setDifficulty(selectedDifficulty);
            }

            if(rankingButton.clicked(mouse.x, mouse.y)){
                app->pushState(new RankingScreen(app, selectedDifficulty));
            }

            if(backButton.clicked(mouse.x, mouse.y)){
                app->popState();
            }

            if(playButton.clicked(mouse.x, mouse.y)){
                if(selectedDifficulty <= unlockedDifficulty){
                    app->changeState(new Playing(app, new Game(selectedDifficulty)));
                }
                else{
                    if(selectedDifficulty == static_cast<int>(DIFFICULTY::MEDIUM)){
                        app->setGlobalMessage(translation(L"You must win an Easy game first!"));
                    }
                    else{
                        app->setGlobalMessage(translation(L"You must win a Medium game first!"));
                    }
                }
            }


            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            nextButton.hover(mouse.x, mouse.y);
            previousButton.hover(mouse.x, mouse.y);

            backButton.hover(mouse.x, mouse.y);
            playButton.hover(mouse.x, mouse.y);
            rankingButton.hover(mouse.x, mouse.y);

            break;
        }
    }

}

void DifficultyMenu::setDifficulty(const int& difficulty){
    if(difficulty == 0){
        title.setString(translation(L"Easy"));
        difficultyBG.setTexture(texturesDAO.getTexture("easy"));
    }
    else if(difficulty == 1){
        title.setString(translation(L"Medium"));
        difficultyBG.setTexture(texturesDAO.getTexture("medium"));
    }
    else{
        title.setString(translation(L"Hard"));
        difficultyBG.setTexture(texturesDAO.getTexture("hard"));
    }

    title.setPosition(SCREEN_LENGHT/2 - title.getGlobalBounds().width/2, title.getPosition().y);
}

void DifficultyMenu::runUnlockedAnimation(const int& unlockedDifficulty){
    selectedDifficulty = unlockedDifficulty;

    if(selectedDifficulty == 0){
        title.setString(translation(L"Easy"));
        difficultyBG.setTexture(texturesDAO.getTexture("easy"));
    }
    else if(selectedDifficulty == 1){
        title.setString(translation(L"Medium"));
        difficultyBG.setTexture(texturesDAO.getTexture("medium"));
    }
    else{
        title.setString(translation(L"Hard"));
        difficultyBG.setTexture(texturesDAO.getTexture("hard"));
    }

    if(unlockedAnimationClock.getElapsedTime().asMilliseconds() < 1500){
        if(animationDelayClock.getElapsedTime().asMilliseconds() >= 50){
            padchange *= -1;
            animationDelayClock.restart();
            padlock.setPosition(padlock.getPosition() + sf::Vector2f(padchange, 0));
        }
    }
    else if(unlockedAnimationClock.getElapsedTime().asMilliseconds() < 2500){
        padlock.setTexture(texturesDAO.getTexture("padlock_open"), true);
    }
    else if(unlockedAnimationClock.getElapsedTime().asMilliseconds() < 3500){
        if(animationDelayClock.getElapsedTime().asMilliseconds() >= 50){
            padOpacity += 12;
            padSize += 0.08f;
            animationDelayClock.restart();
            padlock.setColor(padlock.getColor() - sf::Color(0, 0, 0, padOpacity));
            padlock.setScale(padSize, padSize);
        }
    }
    else{
        showPadlock = false;
        unlockedAnimation = false;
        padlock.setScale(1, 1);
        padlock.setTexture(texturesDAO.getTexture("padlock"), true);
        padlock.setColor(sf::Color(255, 255, 255, 255));
        padlock.setPosition(sf::Vector2f(static_cast<int>(SCREEN_LENGHT*0.5),
                                         static_cast<int>(SCREEN_HEIGHT*0.5)));
    }
}

