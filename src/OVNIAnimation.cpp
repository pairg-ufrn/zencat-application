#include "OVNIAnimation.hpp"

OVNIAnimation::OVNIAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("ovni_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("ovni_2")));

    setPosition(sf::Vector2f(x, y));

    paused = false;

    name = "ovni";

    if(getPosition().x < SCREEN_LENGHT/2){
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0)); //so the sprite do not appear from nowhere
        direction = 1;
        flipX();
    }

    soundPlayer.play("ovni");

}

OVNIAnimation::~OVNIAnimation(){
    //dtor
}
