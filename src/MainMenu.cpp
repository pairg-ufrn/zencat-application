#include "mainMenu.hpp"

MainMenu::MainMenu(Application* app) {
    this->app = app;

    app->statusVerifier.disable();

    background.setTexture(texturesDAO.getTexture("background_main"));

    title.setTexture(texturesDAO.getTexture("logo"));
    title.setPosition(SCREEN_LENGHT*0.5 - title.getLocalBounds().width/2, SCREEN_HEIGHT*0.10);

    aboutButton.setTexture(texturesDAO.getTexture("button_info"));
    aboutButton.setHoverTexture(texturesDAO.getTexture("button_info_hover"));
    aboutButton.setPosition(SCREEN_LENGHT*0.72, SCREEN_HEIGHT*0.83);

    AchievementsButton.setTexture(texturesDAO.getTexture("button_achievements"));
    AchievementsButton.setHoverTexture(texturesDAO.getTexture("button_achievements_hover"));
    AchievementsButton.setPosition(SCREEN_LENGHT*0.5 - AchievementsButton.getSize().x/2, SCREEN_HEIGHT*0.70);

    exitButton.setTexture(texturesDAO.getTexture("button_onoff"));
    exitButton.setHoverTexture(texturesDAO.getTexture("button_onoff_hover"));
    exitButton.setPosition(EXIT_POSITION_X, EXIT_POSITION_Y);

    optionsButton.setTexture(texturesDAO.getTexture("button_gear"));
    optionsButton.setHoverTexture(texturesDAO.getTexture("button_gear_hover"));
    optionsButton.setPosition(SCREEN_LENGHT*0.98 - optionsButton.getSize().x, SCREEN_HEIGHT*0.83);

    playButton.setTexture(texturesDAO.getTexture("button_play"));
    playButton.setHoverTexture(texturesDAO.getTexture("button_play_hover"));
    playButton.setPosition(static_cast<int>(SCREEN_LENGHT*0.5 - playButton.getSize().x/2), static_cast<int>(SCREEN_HEIGHT*0.4));

    generateClouds();
}

void MainMenu::draw(const float dt){
    app->window.draw(background);

    for(auto& animation : animations){
        app->window.draw(*animation);
    }

    app->window.draw(title);
    app->window.draw(playButton);
    app->window.draw(AchievementsButton);
    app->window.draw(optionsButton);
    app->window.draw(aboutButton);
    app->window.draw(exitButton);
    return;
}

void MainMenu::update(const float dt){
    updateAnimations(dt);

    if(generateCloudClock.getElapsedTime().asSeconds() > 15 + rand()%12){
        generateClouds(1);
        generateCloudClock.restart();
    }
}

void MainMenu::updateAnimations(const float& dt){
    std::vector<Animation*>::iterator it = animations.begin();

    while(it != animations.end()){
        if(!(*it)->stillExists()){
            delete (*it);
            it = animations.erase(it);
        }
        else{
            (*it)->update(dt);
            it++;
        }
    }
}

void MainMenu::handleEvent(sf::Event& event){
    switch(event.type){
        case sf::Event::Closed:
            app->window.close();
        break;

         case sf::Event::MouseButtonPressed: {

            int x = event.mouseButton.x;
            int y = event.mouseButton.y;

            sf::Vector2f mouse_world = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            if(playButton.clicked(mouse_world.x, mouse_world.y)){
                app->pushState(new DifficultyMenu(app));
            }
            else if(AchievementsButton.clicked(mouse_world.x, mouse_world.y)){
                app->pushState(new AchievementsScreen(app));
            }
            else if(optionsButton.clicked(mouse_world.x, mouse_world.y)){
                app->pushState(new ConfigMenu(app));
            }
            else if(aboutButton.clicked(mouse_world.x, mouse_world.y)){
                app->pushState(new About(app));
            }
            else if(exitButton.clicked(mouse_world.x, mouse_world.y)){
                app->window.close();
            }
            break;
        }

        case sf::Event::MouseMoved: {
            int x = event.mouseMove.x;
            int y = event.mouseMove.y;

            sf::Vector2f mouse_world = app->window.mapPixelToCoords(sf::Vector2i(x, y));

            AchievementsButton.hover(mouse_world.x, mouse_world.y);
            playButton.hover(mouse_world.x, mouse_world.y);
            optionsButton.hover(mouse_world.x, mouse_world.y);
            aboutButton.hover(mouse_world.x, mouse_world.y);
            exitButton.hover(mouse_world.x, mouse_world.y);

            break;
        }
    }
}


void MainMenu::generateClouds(){
    int bgSize = background.getTexture()->getSize().y;
    int maxHeight = static_cast<int>(bgSize*0.85);
    int initialHeight = static_cast<int>(bgSize*0.10);

    float chance;
    int qtd = 0;

    for(int i = initialHeight; i < maxHeight; i++){
        chance = (rand()%1000)/1000.f;

        if(chance >= 0.992f){
            qtd++;
            float posX = (rand()%100 > 50)? -static_cast<int>(SCREEN_LENGHT*0.45) : SCREEN_LENGHT*1.1;
            int id = 1 + rand()%7;

            animations.push_back(new CloudAnimation(posX, i, 1, id));
        }

    }
    cout << "Generated " << qtd << " clouds." << endl;
}

void MainMenu::generateClouds(const int& qtd){
    int bgSize = background.getTexture()->getSize().y;
    int maxHeight = static_cast<int>(bgSize*0.85);
    int initialHeight = static_cast<int>(bgSize*0.10);

    for(int i = 0; i <= qtd; i++){

        float posX = (rand()%100 > 50)? -static_cast<int>(SCREEN_LENGHT*0.45) : SCREEN_LENGHT*1.1;
        float posY = initialHeight + rand()%(maxHeight);
        int id = 1 + rand()%7;

        animations.push_back(new CloudAnimation(posX, posY, 1, id));
    }

}

