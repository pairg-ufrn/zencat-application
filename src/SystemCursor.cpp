#include "SystemCursor.hpp"

#ifdef SFML_SYSTEM_WINDOWS

SystemCursor::SystemCursor(const SystemCursor::TYPE type){
    switch(type){
        case SystemCursor::WAIT :
            Cursor = LoadCursor(NULL, IDC_WAIT);
        break;

        case SystemCursor::HAND :
            Cursor = LoadCursor(NULL, IDC_HAND);
        break;

        case SystemCursor::NORMAL :
            Cursor = LoadCursor(NULL, IDC_ARROW);
        break;

        case SystemCursor::TEXT :
            Cursor = LoadCursor(NULL, IDC_IBEAM);
        break;

    }
}

void SystemCursor::applyCursor(const sf::WindowHandle& handle) const {
    SetClassLongPtr(handle, GCLP_HCURSOR, reinterpret_cast<LONG_PTR>(Cursor));
}

SystemCursor::~SystemCursor(){
    // Nothing to do for destructor:
    // no memory has been allocated (shared resource principle)
}

#elif defined(SFML_SYSTEM_LINUX)

SystemCursor::SystemCursor(const SystemCursor::TYPE type) {
    display = XOpenDisplay(NULL);

    switch(type){

        case SystemCursor::WAIT:
            Cursor = XCreateFontCursor(display, XC_watch);
        break;

        case SystemCursor::HAND:
            Cursor = XCreateFontCursor(display, XC_hand1);
        break;

        case SystemCursor::NORMAL:
            Cursor = XCreateFontCursor(display, XC_left_ptr);
        break;

        case SystemCursor::TEXT:
            Cursor = XCreateFontCursor(display, XC_xterm);
        break;

    }
}

void SystemCursor::applyCursor(const sf::WindowHandle& handle) const {
    XDefineCursor(display, handle, Cursor);
    XFlush(display);
}

sf::StandardCursor::~StandardCursor() {
    XFreeCursor(display, Cursor);
    delete display;
    display = NULL;
}

#endif
