#include "DragonFlyAnimation.hpp"

DragonFlyAnimation::DragonFlyAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("dragonfly_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("dragonfly_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("dragonfly_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("dragonfly_4")));

    setPosition(sf::Vector2f(x, y));

    paused = false;

    name = "dragonfly";

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        flipX();
    }

    updateTime = sf::milliseconds(35);

    soundPlayer.play("dragonfly");
}

DragonFlyAnimation::~DragonFlyAnimation(){
    //dtor
}
