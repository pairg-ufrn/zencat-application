#include "LanguageDAO.hpp"

#include <boost/locale.hpp>



LanguageDAO& LanguageDAO::getInstance(){
    static LanguageDAO instance;
    return instance;
}


void LanguageDAO::loadLanguage(){

    using namespace boost::locale;

    generator gen;

    std::string languageLocale = configurationDAO.getConfiguration("Language");

    // Specify location of dictionaries
    gen.add_messages_path("./languages/");
    gen.add_messages_domain("locale");

    // Generate locales and imbue them to iostream

    std::locale::global(gen(languageLocale + ".UTF-8"));
    std::cout.imbue(std::locale());

    std::cout << "Loaded language: " << configurationDAO.getConfiguration("Language") << std::endl;

    loadLanguagesLocales();
}

void LanguageDAO::loadLanguagesLocales(){

    std::ifstream file {"\\media\\languages\\locales.dat"};

    languages.clear();

    sf::String languageName;
    std::string languageLocale;
    /*
    while(file >> languageName >> languageLocale){
        languages.push_back(Language{languageName, languageLocale});
    }
    */
    languages.push_back(Language{L"Portugues (BR)", "pt_BR"});
    languages.push_back(Language{L"English (US)",   "en_US"});
    languages.push_back(Language{L"Deutsch",   "de_DE"});
}

std::vector<Language> LanguageDAO::getLanguageList(){
    return languages;
}


sf::String LanguageDAO::getTranslation(const wchar_t* phrase){
    std::wstring translation = boost::locale::translate(phrase);
    sf::String result(translation);
    return result;
}
