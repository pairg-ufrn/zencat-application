#include "LoadingScreen.hpp"

LoadingScreen::LoadingScreen(){
    loadingWindow.create(sf::VideoMode(300, 300), "", sf::Style::None);
    loadingWindow.requestFocus();

    sf::Image backgroundImage;
    backgroundImage.loadFromFile("media\\Textures\\icon_loading.png");

    setShape(loadingWindow.getSystemHandle(), backgroundImage);
    setTransparency(loadingWindow.getSystemHandle());

    logoTexture.loadFromFile("media\\Textures\\icon_loading.png");
    logo.setTexture(logoTexture);

    loadingWindow.clear(sf::Color::Transparent);
    loadingWindow.draw(logo);
    loadingWindow.display();
}

LoadingScreen::~LoadingScreen(){
    //dtor
}

void LoadingScreen::close(){
    loadingWindow.close();
}

void LoadingScreen::setTransparency(HWND handle){
    SetWindowLong(handle, GWL_EXSTYLE, GetWindowLong(handle, GWL_EXSTYLE) | WS_EX_LAYERED);
    SetLayeredWindowAttributes(handle, 0, 255, LWA_ALPHA);
}

void LoadingScreen::setShape(HWND handle, const sf::Image& image){
        const sf::Uint8* pixelData = image.getPixelsPtr();
        HRGN hRegion = CreateRectRgn(0, 0, image.getSize().x, image.getSize().y);

        // Determine the visible region
        for (unsigned int y = 0; y < image.getSize().y; y++){

                for (unsigned int x = 0; x < image.getSize().x; x++){

                        if (pixelData[y * image.getSize().x * 4 + x * 4 + 3] == 0){
                                HRGN hRegionDest = CreateRectRgn(0, 0, 1, 1);
                                HRGN hRegionPixel = CreateRectRgn(x, y, x + 1, y + 1);
                                CombineRgn(hRegionDest, hRegion, hRegionPixel, RGN_XOR);
                                DeleteObject(hRegion);
                                DeleteObject(hRegionPixel);
                                hRegion = hRegionDest;
                        }
                }
        }

        SetWindowRgn(handle, hRegion, true);
        DeleteObject(hRegion);
        return;
}
