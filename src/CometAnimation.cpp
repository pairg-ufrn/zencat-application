#include "CometAnimation.hpp"

CometAnimation::CometAnimation(const float& x, const float& y){
    addFrame(sf::Sprite(texturesDAO.getTexture("comet_1")));
    addFrame(sf::Sprite(texturesDAO.getTexture("comet_2")));
    addFrame(sf::Sprite(texturesDAO.getTexture("comet_3")));
    addFrame(sf::Sprite(texturesDAO.getTexture("comet_4")));

    setPosition(sf::Vector2f(x, y));

    paused = false;

    name = "comet";

    angle = -30 + rand()%60;

    for(auto& frame : frames){
        frame.rotate(angle);
    }

    if(getPosition().x < SCREEN_LENGHT/2){
        direction = 1;
        angle = -angle;
        setPosition(getPosition() - sf::Vector2f(frames.at(0).getTexture()->getSize().x, 0));
        flipX();
    }

    soundPlayer.play("comet");

}

CometAnimation::~CometAnimation(){
    //dtor
}

void CometAnimation::loop(const float& dt){
    setPosition(getPosition() + sf::Vector2f(direction, (angle < 0? 1 : -1)));
}

bool CometAnimation::stillExists(){
    if(getPosition().x <= SCREEN_LENGHT*1.20 and getPosition().x >= (-1)*SCREEN_LENGHT*0.2){
        return true;
    }
    return false;
}
