#include "configurationDAO.hpp"


ConfigurationDAO& ConfigurationDAO::getInstance(){
    static ConfigurationDAO instance;
    return instance;
}

void ConfigurationDAO::loadConfiguration(){
    std::ifstream config {"zencat.cfg"};
    parse(config);
}

std::string ConfigurationDAO::getConfiguration(std::string conf){
    try{
        return options.at(conf);
    }
    catch(const std::out_of_range& err){
        std::cout << "ERROR: Configuration " << conf << " not found or loaded." << std::endl;
        std::runtime_error("ERROR: Configuration " + conf + " not found or loaded.\n");
    }
}


void ConfigurationDAO::parse(std::ifstream & cfgfile){
    std::string id, eq, val;
    while(cfgfile >> id >> eq >> val){
      if (id[0] == '#') continue;  // skip comments
      if (eq != "=") throw std::runtime_error("Parse error");
      val = processOption(val);
      options[id] = val;
    }
}

std::string ConfigurationDAO::processOption(std::string opt){
    std::string result = "";
    int i = 1;
    //The string must begin and end with an _"_ character
    if(opt.at(0) != '\"' or opt.at(opt.size() - 1) != '\"'){
        throw std::runtime_error("Configuration file: invalid format");
    }
    else{
        while(opt[i] != '\"'){
            result += opt[i];
            i++;
        }
    }
    return result;
}

void ConfigurationDAO::setCOMPort(std::string COMPort){
    options["COMPort"] = COMPort;
    saveConfiguration();
}

void ConfigurationDAO::setLanguage(std::string lang){
    options["Language"] = lang;
    saveConfiguration();
}

void ConfigurationDAO::setConsole(int console){
    std::string con = Utility::toString(console);
    options["Console"] = con;
    saveConfiguration();
}

void ConfigurationDAO::setAudio(int audio){
    std::string aud = Utility::toString(audio);
    options["Volume"] = aud;
    saveConfiguration();
}

void ConfigurationDAO::setUser(std::string user){
    options["User"] = user;
    saveConfiguration();
}

void ConfigurationDAO::setUnlocked(int unlocked){
    std::string unlckd = Utility::toString(unlocked);
    options["Unlocked"] = unlckd;
    saveConfiguration();
}


void ConfigurationDAO::saveConfiguration(){
    std::ofstream output{"zencat.cfg"};
    output  << "COMPort = "         << '"' << options["COMPort"]        << '"' << "\n"
            << "Volume = "          << '"' << options["Volume"]         << '"' << "\n"
            << "Language = "        << '"' << options["Language"]       << '"' << "\n"
            << "Console = "         << '"' << options["Console"]        << '"' << "\n"
            << "User = "            << '"' << options["User"]           << '"' << "\n"
            << "GUI = "             << '"' << options["GUI"]            << '"' << "\n"
            << "ManualGame = "      << '"' << options["ManualGame"]     << '"' << "\n"
            << "Unlocked = "        << '"' << options["Unlocked"]       << '"' << "\n"
            << "ExportDatFiles = "  << '"' << options["ExportDatFiles"] << '"' << "\n";
}

