#ifndef TEXTBOX_H
#define TEXTBOX_H

#include <SFML/Graphics.hpp>
#include "fontsDAO.hpp"

enum TEXTPOS{RIGHT = 0, CENTER, LEFT};

class TextBox : public sf::Drawable{
    public:
        TextBox();
        TextBox(int posX, int posY, int sizeX, int sizeY);

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        bool clicked    (float x, float y);
        bool contains   (float x, float y);

        void setSize(float x, float y);
        void setText(const std::string& text);

        void action();

        void select();
        void deselect();
        bool isSelected();

        void setTextColor(sf::Color color);
        void setShownText(std::string input);
        void setPosition(float x, float y);
        void setTextAlignment(TEXTPOS opt);

        void setDigitsOnly(const bool& digitsOnly);

        void removeWhiteSpaces();
        void removeNonDigits();

        void processUserInput(sf::Event& event);

        sf::Vector2f getSize();
        sf::Vector2f getPosition();
        sf::FloatRect getStringSize();
        std::string getString();
        std::string getLastLetters(std::string input, int letters);

    private:

        sf::RectangleShape body;
        sf::RectangleShape underline;
        sf::Vector2f position;
        sf::Vector2f size;

        sf::Sprite background;

        sf::Color textColor;
        sf::Text shownText;
        sf::Text verticalBarText;
        std::string inputText;

        bool selected;

        bool nonDigitsOnly = false;

        sf::Clock blinkBarClock;
        int blinkBarTime = 600; //in milliseconds

        const unsigned int underlineThickness = 2;

};

#endif // TEXTBOX_H
