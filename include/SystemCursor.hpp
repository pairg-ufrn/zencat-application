#ifndef SYSTEMCURSOR_HPP
#define SYSTEMCURSOR_HPP

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#ifdef SFML_SYSTEM_WINDOWS
    #include <windows.h>
#elif defined(SFML_SYSTEM_LINUX)
    #include <X11/cursorfont.h>
    #include <X11/Xlib.h>
#endif

class SystemCursor {

    public:
        enum TYPE { WAIT, TEXT, NORMAL, HAND /*,...*/ };
        SystemCursor(const TYPE type);
        void applyCursor(const sf::WindowHandle& handle) const;
        ~SystemCursor();

    private:
        #ifdef SFML_SYSTEM_WINDOWS

        HCURSOR Cursor;

        #else

        XID Cursor;
        Display* display;

        #endif
};

#endif
