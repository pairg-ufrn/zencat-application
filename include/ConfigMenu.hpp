#ifndef CONFIGMENU_H
#define CONFIGMENU_H

#include "State.hpp"
#include "Button.hpp"
#include "TextBox.hpp"
#include "VolumeBar.hpp"
#include "SystemCursor.hpp"

class ConfigMenu : public State{
    public:
        ConfigMenu(Application* app);
        virtual ~ConfigMenu();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

        void saveConfigurations();

    private:

        sf::Sprite background;

        sf::Sprite frame;

        Button autoConnectButton;
        Button saveButton;
        Button cancelButton;
        Button restoreDefaultsButton;

        Button nextLanguageButton;
        Button previousLanguageButton;

        VolumeBar volumeBar;

        TextBox playerNameTextBox;
        TextBox comPortTextBox;

        sf::Text title;
        sf::Text userTitle;
        sf::Text soundText;
        sf::Text languageTitle;
        sf::Text languageText;
        sf::Text comPortTitle;

        int volume;

        int languageIndex;

        std::vector<Language> languageList;

};


#endif // CONFIGMENU_H
