#ifndef ROCKETANIMATION_HPP
#define ROCKETANIMATION_HPP

#include "Animation.hpp"


class RocketAnimation : public Animation{
    public:
        RocketAnimation(const float& x = 0, const float& y = 0);
        virtual ~RocketAnimation();
        virtual void loop(const float& dt);
        virtual bool stillExists();

    private:
        int direction = -1;
        float angle;
        //sf::Vector2f direction;
};

#endif // ROCKETANIMATION_HPP
