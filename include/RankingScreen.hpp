#ifndef RANKINGSCREEN_HPP
#define RANKINGSCREEN_HPP

#include "State.hpp"
#include "GameDAO.hpp"

typedef struct{
    sf::Text position;
    sf::Text playerName;
    sf::Text matchDate;
    sf::Text avgMeditation;
    sf::Text maxHeight;
    sf::Text performance;
    sf::Sprite frame;

} RankingRow;

class RankingScreen : public State{
    public:
        RankingScreen(Application* app, const int& difficulty);
        virtual ~RankingScreen();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:
        sf::Text title;
        sf::Text rankingPositionTitle;
        sf::Text playerNameTitle;
        sf::Text matchDateTitle;
        sf::Text avgMeditationTitle;
        sf::Text maxHeightTitle;

        Button returnButton;

        sf::Sprite background;
        sf::Sprite frame;
        sf::Sprite tableTop;

        std::vector<RankingRow> rows;

        void formatRows();
        void adjustPlayerNameSize(sf::Text* playerName);
};

#endif // RANKINGSCREEN_HPP
