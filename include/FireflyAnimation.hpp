#ifndef FIREFLYANIMATION_HPP
#define FIREFLYANIMATION_HPP

#include <HorizontalAnimation.hpp>

class FireflyAnimation : public HorizontalAnimation{
    public:
        FireflyAnimation(const float& x = 0, const float& y = 0);
        virtual ~FireflyAnimation();
};

#endif // FIREFLYANIMATION_HPP
