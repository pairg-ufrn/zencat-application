#ifndef ANIMATION_H
#define ANIMATION_H

#include "global.hpp"
#include "SoundPlayer.hpp"

class Animation : public sf::Drawable{
    public:
        Animation(const float& x = 0, const float& y = 0, sf::Time _updateTime = sf::milliseconds(200));
        virtual ~Animation();

        void addFrame(sf::Sprite frame);

        void play();
        void pause();
        void stop();
        void clearFrames();
        std::string getName();
        void update(const float& dt);
        virtual void loop(const float& dt){};
        virtual bool stillExists(){return true;};

        void flipX();
        void unflipX();

        void setPosition(sf::Vector2f pos);
        sf::Vector2f getPosition();

    protected:
        sf::Time duration;

        unsigned int currentFrame;
        std::string name;

        sf::Time updateTime;
        sf::Time currentTime;

        bool paused;

        sf::Time getDuration();

        std::vector<sf::Sprite> frames;
        sf::Vector2f position;

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // ANIMATION_H
