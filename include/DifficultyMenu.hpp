#ifndef DIFFICULTYMENU_H
#define DIFFICULTYMENU_H

#include "State.hpp"
#include "Button.hpp"
#include "Playing.hpp"
#include "RankingScreen.hpp"

#include <SFML/Graphics.hpp>

class DifficultyMenu : public State{
    public:
        DifficultyMenu(Application* app, bool unlockedAnimation = false);
        virtual ~DifficultyMenu();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:

        sf::Text title;

        sf::Sprite background;

        sf::Sprite frame;
        sf::Sprite difficultyBG;
        sf::Sprite padlock;

        Button nextButton;
        Button previousButton;

        Button backButton;
        Button playButton;
        Button rankingButton;

        sf::Clock unlockedAnimationClock;
        sf::Clock animationDelayClock;
        void runUnlockedAnimation(const int& unlockedDifficulty);

        bool unlockedAnimation;
        bool showPadlock = false;

        int selectedDifficulty = 0;
        int unlockedDifficulty = 0;

        void setDifficulty(const int& difficulty);

        //controls the padlock unlock animation
        int padchange = 8;
        int padcontrol = 1;
        int padOpacity = 0;
        float padSize = 1.f;

};

#endif // DIFFICULTYMENU_H
