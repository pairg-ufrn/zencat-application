#ifndef VOLUMEBAR_H
#define VOLUMEBAR_H

#include <SFML/Graphics.hpp>
#include "TexturesDAO.hpp"

class VolumeBar : public sf::Drawable{
    public:
        VolumeBar();
        virtual ~VolumeBar();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        void handleEvent(sf::Event& event);

        void setPosition(sf::Vector2f pos);
        void setVolume(unsigned int vol);

        sf::Vector2u getSize();
        unsigned int getVolume();
    private:

        unsigned int volume;

        sf::Sprite frame;
        sf::Sprite bar;
        sf::Sprite soundIcon;

        sf::Vector2f position;
        sf::Vector2u b_size;

};

#endif // VOLUMEBAR_H
