#ifndef TEXTURESDAO_H
#define TEXTURESDAO_H

#include <iostream>
#include <map>
#include <fstream>
#include <SFML/Graphics.hpp>
#include <boost/filesystem.hpp>
#include <utility.hpp>
#include <thread>

#include "TextureRenderer.hpp"

#ifndef texturesDAO
#define texturesDAO TexturesDAO::getInstance()
#endif

class TexturesDAO{
    public:
        TexturesDAO();
        virtual ~TexturesDAO();

        static TexturesDAO& getInstance();
        sf::Texture& getTexture(const std::string& textureName);

        void loadTextures();

        unsigned int findFiles(std::vector<std::string>& files, const std::string& directory);

        std::string removeFilePath(const std::string& file);

        void loadTexture(const std::string& name, const std::string& filename);

    private:

        static TexturesDAO* instance;

        std::map<std::string, sf::Texture> textures;
};

#endif // TEXTURESDAO_H
