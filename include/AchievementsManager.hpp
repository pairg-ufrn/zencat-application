#ifndef ACHIEVEMENTSMANAGER_H
#define ACHIEVEMENTSMANAGER_H

#ifndef achievementsManager
#define achievementsManager AchievementsManager::getInstance()
#endif

#include <string>
#include <vector>

#include "Game.hpp"

class Game;

struct Achievement {
    bool obtained;
    bool recentlyObtained;
    const std::string title;
    const std::string description;
    const std::string textureName;
    bool (*condition)(Game*);
};

class AchievementsManager{
    public:
        static AchievementsManager& getInstance();
        virtual ~AchievementsManager();

        bool unlockedAchievement();
        void trackAchievements(Game* game);
        void setUnlocked(bool unlocked);
        std::vector<Achievement> getAchievements();

        void saveAchievements();
        void reloadAchievements();

    private:
        static AchievementsManager* instance;

        bool unlockedNewAchievement = false;

        std::vector<Achievement> achievements;

        AchievementsManager();
        void loadAchievements();


};

#endif // ACHIEVEMENTSMANAGER_H
