#ifndef CLOUDANIMATION_HPP
#define CLOUDANIMATION_HPP

#include "HorizontalAnimation.hpp"

class CloudAnimation : public HorizontalAnimation{
    public:
        CloudAnimation(const float& x = 0, const float& y = 0, const int& scenarioID = 1, const int& cloudID = 1);
        virtual ~CloudAnimation();
        virtual void loop(const float& dt);

    private:
        float velocity;
};

#endif // CLOUDANIMATION_HPP
