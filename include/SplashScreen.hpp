#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include "State.hpp"

class SplashScreen : public State{
    public:
        SplashScreen(Application* app);
        virtual ~SplashScreen();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:

        int color = 0;
        int alpha = 0;

        sf::Sprite splash;

        sf::Clock splashClock;

        bool playSong = true;
};

#endif // SPLASHSCREEN_H
