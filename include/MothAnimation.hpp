#ifndef MOTHANIMATION_HPP
#define MOTHANIMATION_HPP

#include "HorizontalAnimation.hpp"


class MothAnimation : public HorizontalAnimation{
    public:
        MothAnimation(const float& x = 0, const float& y = 0);
        virtual ~MothAnimation();
};

#endif // MOTHANIMATION_HPP
