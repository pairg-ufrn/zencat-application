#ifndef CLOCKANIMATION_H
#define CLOCKANIMATION_H

#include "Animation.hpp"

class ClockAnimation : public Animation{
    public:

        ClockAnimation(const float& x = 0, const float& y = 0, sf::Time _totalTime = sf::milliseconds(3600));
        virtual ~ClockAnimation();

        void startClock();

    private:
        unsigned int totalTime;

        sf::Clock remainingTime;
};

#endif // CLOCKANIMATION_H
