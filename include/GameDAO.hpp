#ifndef GAMEDAO_H
#define GAMEDAO_H

#include "Game.hpp"
#include <sstream>
#include <ostream>
#include <istream>

#ifndef gameDAO
#define gameDAO GameDAO::getInstance()
#endif

class Game;

typedef struct {
    std::string playerName;
    std::string matchDate;
    std::string avgMeditation;
    std::string maxHeight;
    std::string performance;
} MatchLog;

class GameDAO{
    public:
        GameDAO();
        virtual ~GameDAO();
        static GameDAO& getInstance();

        void saveGame(Game* game);
        void loadRankings();
        void saveRanking(int difficulty);

        void logMatch(MatchLog match, int difficulty);

        std::vector<MatchLog> getRanking(int difficulty);

    private:
        static GameDAO* instance;
        GameDAO(GameDAO const&);

        void sortMatch(std::vector<MatchLog>* ranking, MatchLog match);
        void removeNewLines(std::string* word);

        std::vector<MatchLog> easyRanking;
        std::vector<MatchLog> mediumRanking;
        std::vector<MatchLog> hardRanking;
};

#endif // GAMEDAO_H
