#ifndef _LANGDAO
#define _LANGDAO
#include <iostream>
#include <map>

#include "ConfigurationDAO.hpp"
#include <SFML/Graphics.hpp>
#include <windows.h>

#include <boost/locale.hpp>

#ifndef languageDAO
#define languageDAO LanguageDAO::getInstance()
#endif

typedef struct {

    std::wstring languageName;
    std::string languageLocale;

} Language;

class LanguageDAO{

    public:
        static LanguageDAO& getInstance();

        sf::String getTranslation(const wchar_t* phrase);

        void loadLanguage();

        std::vector<Language> getLanguageList();

    private:
        static LanguageDAO* instance;
        LanguageDAO(LanguageDAO const&);

        void loadLanguagesLocales();

        std::vector<Language> languages;

        LanguageDAO(){};

};

#endif // _LANGDAO
