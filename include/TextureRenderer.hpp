#ifndef TEXTURERENDERER_HPP
#define TEXTURERENDERER_HPP

#include <SFML/Graphics.hpp>
#include "Global.hpp"

class TextureRenderer{
    public:
        TextureRenderer();
        virtual ~TextureRenderer();

        void rescaleTexture(sf::Texture* texture, const unsigned int& size_x, const unsigned int& size_y);
        void renderTexture(sf::Texture* texture, sf::RenderTexture renderedTexture);
    private:
};

#endif // TEXTURERENDERER_HPP
