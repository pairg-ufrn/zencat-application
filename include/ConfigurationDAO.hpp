#ifndef _CONFDAO
#define _CONFDAO
#include <iostream>
#include <map>
#include <fstream>
#include "utility.hpp"
#include <stdexcept>

#ifndef configurationDAO
#define configurationDAO ConfigurationDAO::getInstance()
#endif

class ConfigurationDAO{
    public:
        static ConfigurationDAO& getInstance();

        std::string getConfiguration(std::string conf);

        void setCOMPort(std::string COMPort);
        void setAudio(int audio);
        void setConsole(int console);
        void setUnlocked(int unlocked);
        void setLanguage(std::string lang);
        void setUser(std::string user);

    private:
        static ConfigurationDAO* instance;
        ConfigurationDAO(ConfigurationDAO const&);
        void operator=(ConfigurationDAO const&);

        std::map<std::string, std::string> options;

        std::string processOption(std::string opt);

        void loadConfiguration();
        void saveConfiguration();
        void parse(std::ifstream & cfgfile);

        ConfigurationDAO(){
            loadConfiguration();
        };


};

#endif // _CONFDAO
