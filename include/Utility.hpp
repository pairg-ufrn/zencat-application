#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <global.hpp>
#include <sstream>
#include <chrono>
#include <ctime>
#include <string>
#include <istream>
#include <fstream>

using namespace std::chrono;

#define padIt(_x_) (now_in._x_ < 10 ? "0" : "") << now_in._x_

class Utility{
    public:
    static bool isEmpty(std::ifstream& pFile);

    static int StringToNumber (const std::string& Text);

    static bool isDigit(const char& digit);

    static std::string toClock(double time);

    static std::string toString (const double& number);

    static std::string getDatestamp (std::string separator);

    static std::string getYear();
    static std::string getMonth();
    static std::string getDay();

    static std::string getTimestamp ();

    static std::string getTimestamp (std::string separator);

    static std::string getDatestamp (bool date, bool hour);

    static std::string encryptDecrypt (std::string phrase);

};

#endif // UTILITY_HPP
