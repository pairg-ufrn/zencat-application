#ifndef STATUSVERIFIER_H
#define STATUSVERIFIER_H

#include <SFML/Graphics.hpp>
#include "MindwaveModule.hpp"
#include "TexturesDAO.hpp"

class StatusVerifier : public sf::Drawable{
    public:
        StatusVerifier();
        virtual ~StatusVerifier();

        void loop();
        void handleEvent(sf::Event& event);
        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        void checkHeadsetStatus();

        void enable();
        void disable();

    private:

    sf::Sprite frame;

    sf::Sprite           batteryStatus;
    sf::Sprite headsetConnectionStatus;
    sf::Sprite     headsetSignalStatus;
    sf::Sprite    headsetBatteryStatus;

    sf::Clock checkHeadsetConnectionClock;

    bool enabled = true;
};

#endif // STATUSVERIFIER_H
