#ifndef LEAFANIMATION_HPP
#define LEAFANIMATION_HPP

#include "HorizontalAnimation.hpp"


class LeafAnimation : public HorizontalAnimation {
    public:
        LeafAnimation(const float& x = 0, const float& y = 0);
        virtual ~LeafAnimation();
        virtual void loop(const float& dt);
};

#endif // LEAFANIMATION_HPP
