#ifndef BIRDANIMATION_H
#define BIRDANIMATION_H

#include "HorizontalAnimation.hpp"

class BirdAnimation : public HorizontalAnimation{
    public:
        BirdAnimation(const float& x = 0, const float& y = 0);
        virtual ~BirdAnimation();
};

#endif // BIRDANIMATION_H
