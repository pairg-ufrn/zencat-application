#ifndef _PLAYING
#define _PLAYING

#include "State.hpp"
#include "game.hpp"
#include "GameDAO.hpp"
#include "AchievementsManager.hpp"

class Game;

class Playing : public State{
    private:
        Application* app;
        Game* game;

    public:
        virtual void draw(const float dt);
        virtual void handleEvent(sf::Event& event);
        virtual void update(const float dt);

        Playing(Application* app, Game* game);
        ~Playing();
};

#endif // _PLAYING

