#ifndef GAME_H
#define GAME_H

#include "Application.hpp"
#include "DifficultyMenu.hpp"

#include "Animation.hpp"
#include "AirplaneAnimation.hpp"
#include "AirshipAnimation.hpp"
#include "BalloonAnimation.hpp"
#include "BirdAnimation.hpp"
#include "ClockAnimation.hpp"
#include "CloudAnimation.hpp"
#include "CometAnimation.hpp"
#include "DragonFlyAnimation.hpp"
#include "FireflyAnimation.hpp"
#include "FishAnimation.hpp"
#include "GameOverScreen.hpp"
#include "IncenseAnimation.hpp"
#include "LeafAnimation.hpp"
#include "MothAnimation.hpp"
#include "OVNIAnimation.hpp"
#include "RocketAnimation.hpp"
#include "StarAnimation.hpp"
#include "WaterfallAnimation.hpp"

#include "Button.hpp"
#include "MindwaveModule.hpp"
#include <SFML/Graphics.hpp>

class Game{
    public:
        Game(int difficulty);
        ~Game();

        Application* app;

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

        virtual void updateGame(const float dt);

        void setApp(Application* app);


        int              getResult();
        int              getDuration();
        int              getDifficulty();
        int              getBestHeight();
        int              getPerformance();
        int              getPlayerHeight();
        int              getMaximumHeight();
        std::vector<int> getAttentionValues();
        std::vector<int> getMeditationValues();
        float            getHeightPercentage();
        int              getUnlockedNewDifficulty();
        void             setUnlockedNewDifficulty(const bool& unlocked);
        float            getAverageMeditation(const int& lastValues = 0);

        GAMESTATE        getGameState();

    protected:

        GAMESTATE GameState;

        ClockAnimation* gameClockAnimation;

        sf::Text alertText;
        sf::Text counterText;
        sf::Text gameClockText;

        sf::Sprite background;

        sf::Sprite topFrame;
        sf::Sprite meditationBar;
        sf::Sprite meditationFrame;
        sf::Sprite meditationLimit;
        sf::Sprite meditationMean;
        sf::Sprite heightMeter;

        sf::Sprite counterSprite;

        sf::Sprite cat;
        sf::Sprite pillow;

        sf::Sprite trees;

        std::vector<Animation*> animations;

        bool manualGame = false;
        bool gameover = false;
        bool rolling = false;
        bool applyPenalty = false;
        bool unlockedNewDifficulty = false;

        int result = 0;
        int performance = 1;
        int difficulty;

        int force;
        int initialHeight;
        int maximumHeight;
        int baseLimit = 40;
        int bestHeight = 0;
        int minimumLimit = 40;

        int currentAverageMeditation = 0;

        sf::Clock startTime;
        sf::Clock gameClock;
        sf::Clock updateClock;
        sf::Clock counterClock;
        sf::Clock penaltyClock;
        sf::Clock createAnimationClock;
        sf::Clock increaseMinimumAverageClock;

        sf::View UIView;
        sf::View gameView;

        int attention, meditation, blinkStrength, resultant;
        std::vector<int> attentionValues;
        std::vector<int> meditationValues;

        void generateClouds();
        void createAnimations();
        void updateAnimations(float dt);

        virtual void testGameover();
        void setGameover();

        void drawGame();

        bool connectionLost = false;

        /* Paused state */
        void drawPause();

        bool paused = false;
        int pausedTime = 0;
        sf::Clock pauseClock;
        sf::Clock returnTime;

        sf::Sprite pauseFrame;
        sf::Sprite pauseBackground;

        sf::Text pauseTitle;
        sf::Text pauseText;

        /* Confirm exit */

        bool confirmExit = false;

        sf::Sprite confirmExitFrame;
        sf::Text confirmExitText;
        sf::Text confirmExitOptions;

};

#endif // GAME_H
