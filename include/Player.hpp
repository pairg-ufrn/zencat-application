#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <global.hpp>
#include <SFML/Graphics.hpp>
#include <Animation.hpp>

class Player : public sf::Drawable{
    public:
        Player();
        virtual ~Player();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        virtual void update();
        void updateAnimations(float dt);

        int getForce();
        void setForce(int _force);
        void addForce(int _force);
        void applyForce(sf::Vector2f _force);

        void blink();
        void logBlink(int blinkStrength);

        float getAverageForce();
        std::vector<int> getForceValues();

        std::string getName();
        void setName(std::string name);

        void setPosition(sf::Vector2f newPos);
        sf::Vector2f getPosition();

    protected:
        int ID;
        std::string name;
        std::vector<int> forceValues;
        int force;
        int blinkCharges;
        int updateForce;

        sf::Clock updateForceClock;

        sf::Sprite p_sprite;

        sf::RectangleShape p_body;

        sf::Vector2f p_position;
};

#endif // PLAYER_H
