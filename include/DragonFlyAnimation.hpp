#ifndef DRAGONFLYANIMATION_H
#define DRAGONFLYANIMATION_H

#include "HorizontalAnimation.hpp"

class DragonFlyAnimation : public HorizontalAnimation{
    public:
        DragonFlyAnimation(const float& x = 0, const float& y = 0);
        virtual ~DragonFlyAnimation();
};

#endif // DRAGONFLYANIMATION_H
