#ifndef _GLOBAL
#define _GLOBAL

#include <clocale>

#include <iostream>
#include <sstream>

#include "ConfigurationDAO.hpp"
#include "LanguageDAO.hpp"
#include "TexturesDAO.hpp"
#include "FontsDAO.hpp"
#include "SoundsDAO.hpp"
#include "SoundPlayer.hpp"

//#define SCREEN_LENGHT 512
//#define SCREEN_HEIGHT 700

#define PLAYER_SIZE 96

#define GAME_TITLE "Zen Cat"

#define FRAMERATE 60

#define MAX_FORCE 100

#define MIN_FORCE 0

#define LAST_VALUES 15

#define translation(str) languageDAO.getTranslation(str)

const std::string GAME_VERSION = "0.1.2";

const int SCREEN_LENGHT = 800;
const int SCREEN_HEIGHT = 600;

const int EXIT_POSITION_X = SCREEN_LENGHT*0.02;
const int EXIT_POSITION_Y = SCREEN_HEIGHT*0.83;

enum class GAMESTATE {COUNTDOWN = 0, PLAYING, OVER, PAUSED};

enum class DIFFICULTY {EASY = 0, MEDIUM, HARD};

#endif // _GLOBAL

