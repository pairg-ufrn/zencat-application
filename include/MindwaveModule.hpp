#ifndef MINDWAVEMODULE_H
#define MINDWAVEMODULE_H

#include <thinkgear.h>
#include <iostream>
#include <sstream>
#include <string.h>
using namespace std;

#ifndef mindwaveModule
#define mindwaveModule MindwaveModule::getInstance()
#endif

class MindwaveModule{
    public:
        MindwaveModule();
        virtual ~MindwaveModule();
        static MindwaveModule& getInstance();

        int autoConnect();

        int connect(const char* comPortName);
        void disconnect();

        int getConnectionID();

        bool isConnected();
        bool isUpdating();

        void enableBlinkDetection();
        void enableAutoReading();

        int getBlinkStrength();

        int getAttention();
        int getMeditation();

        int getRaw();

        int getAlpha1();
        int getAlpha2();
        int getBeta1 ();
        int getBeta2 ();
        int getGamma1();
        int getGamma2();
        int getDelta ();
        int getTheta ();

        int getLastAttentionValue();
        int getLastMeditationValue();

        void poll();

        int getBatteryStrength();
        int getSignalStrength();
        bool hasPoorSignal();

    private:
        static MindwaveModule* instance;
        int connectionID;
        char* comPort;

        int lastAttentionValue;
        int lastMeditationValue;

        int lastBlinkValue;

        int lastRawValue;

        int lastAlpha1Value;
        int lastAlpha2Value;

        int lastBeta1Value;
        int lastBeta2Value;

        int lastGamma1Value;
        int lastGamma2Value;

        int lastThetaValue;
        int lastDeltaValue;

        std::string toString(const int& number);

};

#endif // MINDWAVEMODULE_H
