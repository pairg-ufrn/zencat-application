#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>

#include "global.hpp"

class Button : public sf::Drawable{
    public:
        Button();
        Button(int x, int y, int size_x, int size_y, std::string desc, bool centerHorizontally = false, bool centerVertically = false);
        virtual ~Button();

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        bool clicked    (const float& x, const float& y);
        bool contains   (const float& x, const float& y);
        bool isEnabled  ();
        void action     ();

        virtual bool hover(const float& x, const float& y);

        void enable         ();
        void disable        ();
        void disableHover   ();
        void setSizeToText  ();
        void setSize        (const float& x, const float& y);
        void setPosition    (const float& x, const float& y);
        void setPosition    (const sf::Vector2f& position);
        void setText        (const std::string& text);
        void setFont        (const sf::Font& font);
        void setColor       (const sf::Color& color);
        void setTexture     (const sf::Texture& texture);
        void setHoverTexture(const sf::Texture& texture);
        void setInvisible   (const bool& inv);

        void alignToLeft    ();
        void alignToRight   ();
        void alignToTop     ();
        void alignToBottom  ();
        void alignToCenter  ();

        void setVerticalPosition(const unsigned int& yPos);
        void setHorizontalPosition(const unsigned int& xPos);

        sf::Vector2f getPosition();
        sf::Vector2f getSize();

        void setFontSize        (const unsigned int& _size);
        void setTextColor       (sf::Color color);
        void setTextColor       (const UINT8& red, const UINT8& green, const UINT8& blue, const UINT8& alpha = 255);
        void updateTextPosition ();

    protected:
        bool isInvisible = false;
        bool enabled = true;
        bool hovered = false;
        bool changeOnHover = true;
        sf::Color hoverColor = sf::Color::Green;
    private:
        sf::RectangleShape b_body;
        sf::Vector2f b_size;
        sf::Vector2f b_position;

        sf::Texture b_texture;
        sf::Texture b_hoverTexture;
        sf::Sprite b_sprite;

        std::string b_string;
        sf::Text b_text;
        sf::Font b_font;

        sf::Color b_color;
};

#endif // BUTTON_H
