#ifndef BALLOONANIMATION_H
#define BALLOONANIMATION_H

#include "HorizontalAnimation.hpp"

class BalloonAnimation  : public HorizontalAnimation{
    public:

        BalloonAnimation(const float& x = 0, const float& y = 0);
        virtual ~BalloonAnimation();

};

#endif // BALLOONANIMATION_H
