#ifndef INCENSEANIMATION_HPP
#define INCENSEANIMATION_HPP

#include "Animation.hpp"

class IncenseAnimation : public Animation{
    public:
        IncenseAnimation(const float& x = 0, const float& y = 0);
        virtual ~IncenseAnimation();
};

#endif // INCENSEANIMATION_HPP
