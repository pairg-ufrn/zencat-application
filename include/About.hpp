#ifndef ABOUT_H
#define ABOUT_H

#include <windows.h>
#include <ShellApi.h>
#include <SFML/Network.hpp>
#include "State.hpp"
#include "Button.hpp"
#include "SystemCursor.hpp"

class About : public State{
    public:
        About(Application* app);
        ~About();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:

        bool compareServerVersion(std::string& serverVersion);

        std::string versionString;

        Button backButton;

        Button UFRNButton;
        Button PAIRGButton;
        Button URLButton;
        Button checkVersionButton;

        sf::Text title;
        sf::Text version;
        sf::Text gameTitle;

        sf::Sprite background;
        sf::Sprite icon;
        sf::Sprite cat;
};

#endif // ABOUT_H
