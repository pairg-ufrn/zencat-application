#ifndef STARANIMATION_HPP
#define STARANIMATION_HPP

#include <Animation.hpp>


class StarAnimation : public Animation{
    public:
        StarAnimation(const float& x = 0, const float& y = 0);
        virtual ~StarAnimation();
        virtual void loop(const float& dt);
        virtual bool stillExists();

    private:
        sf::Clock aliveTime;
        int direction = -1;
        int angle = 0;
};

#endif // STARANIMATION_HPP
