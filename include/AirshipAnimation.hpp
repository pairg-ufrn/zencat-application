#ifndef AIRSHIPANIMATION_H
#define AIRSHIPANIMATION_H

#include "HorizontalAnimation.hpp"

class AirshipAnimation : public HorizontalAnimation{
    public:
        AirshipAnimation(const float& x = 0, const float& y = 0);
        virtual ~AirshipAnimation();
        virtual void loop(const float& dt);

};

#endif // AIRSHIPANIMATION_H
