#ifndef _APP
#define _APP

#include <stack>
#include <iostream>

#include <SFML/Graphics.hpp>

#include "MindwaveModule.hpp"

#include "State.hpp"
#include "Utility.hpp"
#include "Global.hpp"
#include "SoundPlayer.hpp"
#include "StatusVerifier.hpp"

class State;

class Application{
    public:

    std::stack<State*> states;

    sf::RenderWindow window;

    void pushState(State* state);
    void popState();
    void changeState(State* state);
    State* peekState();

    void mainLoop();
    void handleEvent(sf::Event& event);
    void draw();

    void setGlobalMessage(const std::string& message);
    void takeScreenshot();

    Application();
    ~Application();

    StatusVerifier statusVerifier;

    private:

        sf::View applicationView;
        sf::View defaultView;

        sf::Text globalWarning;

        sf::Clock globalWarningClock;

};
#endif // _APP
