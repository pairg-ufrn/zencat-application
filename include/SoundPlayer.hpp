#ifndef SOUNDPLAYER_H
#define SOUNDPLAYER_H

#include <soundsDAO.hpp>

#ifndef soundPlayer
#define soundPlayer SoundPlayer::getInstance()
#endif

class SoundPlayer{
    public:
        SoundPlayer();
        virtual ~SoundPlayer();

        static SoundPlayer& getInstance();

        void play(const std::string& sound, const bool& loop = false);
        void stop(const std::string& sound);
        void stopAll();
        void pauseAll();
        void setVolume(const std::string& sound, const float& volume);
        void setGlobalVolume(const float& volume);
        void setPosition(const std::string& sound, const float& x, const float& y);
        void setAttenuation(const std::string& sound, const int& attenuation);
        void setMinimumDistance(const std::string& sound, const int& distance);
        void mute();

    private:
        static SoundPlayer* instance;

        std::map<std::string, sf::Sound> sounds;
        void initializeSounds();

};

#endif // SOUNDPLAYER_H
