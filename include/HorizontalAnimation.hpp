#ifndef HORIZONTALANIMATION_H
#define HORIZONTALANIMATION_H

#include <Animation.hpp>


class HorizontalAnimation : public Animation{
    public:
        HorizontalAnimation(const float& x = 0, const float& y = 0);
        virtual ~HorizontalAnimation();
        virtual void loop(const float& dt);
        virtual bool stillExists();

    protected:
        int direction = -1;

};

#endif // HORIZONTALANIMATION_H
