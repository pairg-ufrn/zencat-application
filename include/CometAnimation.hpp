#ifndef COMETANIMATION_HPP
#define COMETANIMATION_HPP

#include <Animation.hpp>


class CometAnimation : public Animation{
     public:
        CometAnimation(const float& x = 0, const float& y = 0);
        virtual ~CometAnimation();
        virtual void loop(const float& dt);
        virtual bool stillExists();

    private:
        int direction = -1;
        int angle = 0;
};

#endif // COMETANIMATION_HPP
