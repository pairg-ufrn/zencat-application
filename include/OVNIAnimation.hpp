#ifndef OVNIANIMATION_HPP
#define OVNIANIMATION_HPP

#include <HorizontalAnimation.hpp>


class OVNIAnimation : public HorizontalAnimation{
    public:
        OVNIAnimation(const float& x = 0, const float& y = 0);
        virtual ~OVNIAnimation();
};

#endif // OVNIANIMATION_HPP
