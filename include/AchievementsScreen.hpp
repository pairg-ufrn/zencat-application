#ifndef ACHIEVEMENTSSCREEN_H
#define ACHIEVEMENTSSCREEN_H

#include "State.hpp"
#include "Button.hpp"
#include "AchievementsManager.hpp"

class AchievementsScreen : public State {
    public:
        AchievementsScreen(Application* app);
        virtual ~AchievementsScreen();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:
        Button returnButton;

        sf::Text title;
        sf::Text achievementTitle;
        sf::Text achievementDescription;

        sf::Sprite background;
        sf::Sprite frame;
        sf::Sprite descriptionFrame;
        sf::Sprite darkBackground;

        std::vector<Achievement> achievements;

        std::vector<Button> buttons;

        sf::Sprite notification;


        void openAchievementDescription(const Achievement& achiev);

        bool showDescription = false;
};

#endif // ACHIEVEMENTSSCREEN_H
