#ifndef WATERFALLANIMATION_HPP
#define WATERFALLANIMATION_HPP

#include "Animation.hpp"


class WaterfallAnimation : public Animation{
    public:
        WaterfallAnimation(const float& x = 0, const float& y = 0);
        virtual ~WaterfallAnimation();
    protected:
    private:
};

#endif // WATERFALLANIMATION_HPP
