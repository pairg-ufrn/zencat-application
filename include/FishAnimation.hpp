#ifndef FISHANIMATION_HPP
#define FISHANIMATION_HPP

#include "Animation.hpp"


class FishAnimation : public Animation{
    public:
        FishAnimation(const float& x = 0, const float& y = 0, const int& direction = -1);
        virtual ~FishAnimation();
        virtual void loop(const float& dt);
        virtual bool stillExists();

    private:
        sf::Clock aliveTime;
        int direction = -1;
};

#endif // FISHANIMATION_HPP
