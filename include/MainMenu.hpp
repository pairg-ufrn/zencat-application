#ifndef MAINMENU_HPP
#define MAINMENU_HPP

#include "State.hpp"
#include "button.hpp"
#include "AchievementsScreen.hpp"
#include "difficultyMenu.hpp"
#include "ConfigMenu.hpp"
#include "About.hpp"

#include <SFML/Graphics.hpp>

class MainMenu : public State {
    public:
        MainMenu(Application* app);

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

        void updateAnimations(const float& dt);

        void generateClouds();
        void generateClouds(const int& qtd);

    private:
        sf::Sprite title;

        sf::Sprite background;

        Button playButton;

        Button AchievementsButton;

        Button optionsButton;

        Button aboutButton;

        Button exitButton;

        std::vector<Animation*> animations;

        sf::Clock generateCloudClock;

};

#endif // MAINMENU_HPP
