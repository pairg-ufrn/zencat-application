#ifndef AIRPLANEANIMATION_HPP
#define AIRPLANEANIMATION_HPP

#include <HorizontalAnimation.hpp>


class AirplaneAnimation : public HorizontalAnimation{
    public:
        AirplaneAnimation(const float& x = 0, const float& y = 0);
        virtual ~AirplaneAnimation();
        virtual void loop(const float& dt);
};

#endif // AIRPLANEANIMATION_HPP
