#ifndef LOADINGSCREEN_HPP
#define LOADINGSCREEN_HPP

#include "State.hpp"
#include <SFML/Graphics.hpp>

class LoadingScreen {
    public:

        LoadingScreen();
        virtual ~LoadingScreen();

        void setLoading(bool load);
        void close();

    private:

        sf::RenderWindow loadingWindow;
        sf::Sprite logo;
        sf::Text loadingText;
        sf::Texture logoTexture;

        void setTransparency(HWND handle);
        void setShape(HWND handle, const sf::Image& image);
};

#endif // LOADINGSCREEN_HPP
