#ifndef GAMEOVERSCREEN_HPP
#define GAMEOVERSCREEN_HPP

#include "State.hpp"
#include "Game.hpp"

class Game;

class GameOverScreen : public State{
    public:
        GameOverScreen(Game* game);
        virtual ~GameOverScreen();

        virtual void draw(const float dt);
        virtual void update(const float dt);
        virtual void handleEvent(sf::Event& event);

    private:

        bool updateScoreAnimation = false;
        int updateScoreIndex;
        int performance;

        Button returnButton;
        Button playAgainButton;

        Game* game;

        sf::Clock updateScoreClock;

        sf::Text performanceText;
        sf::Text gameOverText;
        sf::Text achievementText;

        sf::Sprite background;
        sf::Sprite performanceScoreFrame;
        sf::Sprite performanceScoreFace;

        void setPerformanceSprite(int _performance_);

};

#endif // GAMEOVERSCREEN_HPP
